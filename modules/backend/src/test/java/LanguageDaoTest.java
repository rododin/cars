/*
 * LanguageDaoTest.java
 */

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.rododin.cars.backend.BackEnd;
import org.rododin.cars.backend.daos.impl.dictionaries.LanguageDao;
import org.rododin.cars.backend.entities.impl.dictionaries.LanguageEntity;
import org.rododin.cars.generic.log.Log;

/**
 * Description.
 * @author Rod Odin
 */
public class LanguageDaoTest
{
	private LanguageDao languageDao;

	@Before
	public void init()
	{
		languageDao = BackEnd.get().getDao(LanguageDao.class);
	}

	@Test
	public void testSomeLanguagesAreLoaded()
	{
		final Collection<LanguageEntity> allLanguages = languageDao.getAll();
		final Set<String> loadedLanguages = allLanguages.stream().map(LanguageEntity::getIsoCode2).collect(Collectors.toSet());

		Log.debugForced("Loaded Languages: " + allLanguages.stream().map(l -> "\n  " + l).collect(Collectors.toList()));
		//Log.debugForced("Loaded Languages: " + loadedLanguages);

		Assert.assertTrue(loadedLanguages.contains("ru"));
		Assert.assertTrue(loadedLanguages.contains("zh"));
		Assert.assertTrue(loadedLanguages.contains("en"));
		Assert.assertTrue(loadedLanguages.contains("de"));
		Assert.assertTrue(loadedLanguages.contains("fr"));
	}

	@Test
	public void testCRUD()
	{
		// Creating a new language ---------------------------------------------------------------------

		final BackEnd backEnd = BackEnd.get();
		final LanguageEntity newLanguage = new LanguageEntity("er", "ett", "Etruscan", "mekh Rasnal");
		languageDao.serialize(newLanguage);

		Assert.assertNotNull(newLanguage.getId());

		Log.debugForced("New Language added: " + newLanguage);

		// Reading the language ------------------------------------------------------------------------

		languageDao.init(backEnd);
		final LanguageEntity reloadedLanguage = languageDao.get(newLanguage.getId());

		Assert.assertEquals(newLanguage.getIsoCode2(), reloadedLanguage.getIsoCode2());
		Assert.assertEquals(newLanguage.getIsoCode3(), reloadedLanguage.getIsoCode3());
		Assert.assertEquals(newLanguage.getNameEn  (), reloadedLanguage.getNameEn  ());
		Assert.assertEquals(newLanguage.getNameOrig(), reloadedLanguage.getNameOrig());

		Log.debugForced("The Language reloaded: " + reloadedLanguage);

		// Updating the language -----------------------------------------------------------------------

		reloadedLanguage.setNameOrig("Мех РА знал");
		languageDao.serialize(reloadedLanguage);
		languageDao.init(backEnd);
		final LanguageEntity updatedLanguage = languageDao.get(reloadedLanguage.getId());

		Assert.assertEquals(reloadedLanguage.getIsoCode2(), updatedLanguage.getIsoCode2());
		Assert.assertEquals(reloadedLanguage.getIsoCode3(), updatedLanguage.getIsoCode3());
		Assert.assertEquals(reloadedLanguage.getNameEn  (), updatedLanguage.getNameEn  ());
		Assert.assertEquals(reloadedLanguage.getNameOrig(), updatedLanguage.getNameOrig());

		Log.debugForced("The Language updated: " + updatedLanguage);

		// Deleting the language -----------------------------------------------------------------------

		updatedLanguage.setRemoved();
		languageDao.serialize(updatedLanguage);
		languageDao.init(backEnd);
		final LanguageEntity deletedLanguage = languageDao.get(updatedLanguage.getId());

		Assert.assertNull(deletedLanguage);

		Log.debugForced("The Language deleted: " + updatedLanguage);
	}
}
