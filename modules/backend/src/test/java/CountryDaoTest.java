/*
 * CountryDaoTest.java
 */

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.rododin.cars.backend.BackEnd;
import org.rododin.cars.backend.daos.impl.dictionaries.CountryDao;
import org.rododin.cars.backend.entities.impl.dictionaries.CountryEntity;
import org.rododin.cars.backend.entities.impl.dictionaries.LanguageEntity;
import org.rododin.cars.generic.log.Log;

/**
 * Description.
 * @author Rod Odin
 */
public class CountryDaoTest
{
	private CountryDao countryDao;

	@Before
	public void init()
	{
		countryDao = BackEnd.get().getDao(CountryDao.class);
	}

	@Test
	public void testSomeCountriesAreLoaded()
	{
		final Collection<CountryEntity> allCountries = countryDao.getAll();
		final Set<String> loadedCountries = allCountries.stream().map(CountryEntity::getIsoCode2).collect(Collectors.toSet());

		Log.debugForced("Loaded Countries: " + allCountries.stream().map(c -> "\n  " + c).collect(Collectors.toList()));

		Assert.assertTrue(loadedCountries.contains("RU"));
		Assert.assertTrue(loadedCountries.contains("CN"));
		Assert.assertTrue(loadedCountries.contains("US"));
		Assert.assertTrue(loadedCountries.contains("GB"));
		Assert.assertTrue(loadedCountries.contains("DE"));
		Assert.assertTrue(loadedCountries.contains("FR"));
	}

	@Test
	public void testSomeCountryLanguagesAreLoaded()
	{
		final Set<String> loadedCountryLanguages = countryDao.getAll().stream().map(country -> country.getIsoCode2() + "-" + country.getDefaultLanguage().getIsoCode2()).collect(Collectors.toSet());

		Assert.assertTrue(loadedCountryLanguages.contains("RU-ru"));
		Assert.assertTrue(loadedCountryLanguages.contains("CN-zh"));
		Assert.assertTrue(loadedCountryLanguages.contains("US-en"));
		Assert.assertTrue(loadedCountryLanguages.contains("GB-en"));
		Assert.assertTrue(loadedCountryLanguages.contains("DE-de"));
		Assert.assertTrue(loadedCountryLanguages.contains("FR-fr"));
	}

	@Test
	public void testCRUD()
	{
		// Creating a new country ----------------------------------------------------------------------

		final BackEnd backEnd = BackEnd.get();
		final CountryEntity newCountry = new CountryEntity("SU", "SUN", "USSR", "СССР", LanguageEntity.DEFAULT_LANGUAGE);
		countryDao.serialize(newCountry);

		Assert.assertNotNull(newCountry.getId());

		Log.debugForced("New Country added: " + newCountry);

		// Reading the country -------------------------------------------------------------------------

		countryDao.init(backEnd);
		final CountryEntity reloadedCountry = countryDao.get(newCountry.getId());

		Assert.assertEquals(newCountry.getIsoCode2(), reloadedCountry.getIsoCode2());
		Assert.assertEquals(newCountry.getIsoCode3(), reloadedCountry.getIsoCode3());
		Assert.assertEquals(newCountry.getNameEn  (), reloadedCountry.getNameEn  ());
		Assert.assertEquals(newCountry.getNameOrig(), reloadedCountry.getNameOrig());

		Log.debugForced("The Country reloaded: " + reloadedCountry);

		// Updating the country ------------------------------------------------------------------------

		reloadedCountry.setNameOrig("Советский Союз");
		countryDao.serialize(reloadedCountry);
		countryDao.init(backEnd);
		final CountryEntity updatedCountry = countryDao.get(reloadedCountry.getId());

		Assert.assertEquals(reloadedCountry.getIsoCode2(), updatedCountry.getIsoCode2());
		Assert.assertEquals(reloadedCountry.getIsoCode3(), updatedCountry.getIsoCode3());
		Assert.assertEquals(reloadedCountry.getNameEn  (), updatedCountry.getNameEn  ());
		Assert.assertEquals(reloadedCountry.getNameOrig(), updatedCountry.getNameOrig());

		Log.debugForced("The Country updated: " + updatedCountry);

		// Deleting the country -----------------------------------------------------------------------

		updatedCountry.setRemoved();
		countryDao.serialize(updatedCountry);
		countryDao.init(backEnd);
		final CountryEntity deletedCountry = countryDao.get(updatedCountry.getId());

		Assert.assertNull(deletedCountry);

		Log.debugForced("The Country deleted: " + updatedCountry);
	}
}
