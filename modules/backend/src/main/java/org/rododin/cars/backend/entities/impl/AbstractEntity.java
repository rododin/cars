/*
 * AbstractEntity.java
 */

package org.rododin.cars.backend.entities.impl;

import org.rododin.cars.backend.entities.Entity;

/**
 * Description.
 * @author Rod Odin
 */
public class AbstractEntity<Id>
	implements Entity<Id>
{
// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractEntity()
	{
		this (null);
	}

	protected AbstractEntity(Id id)
	{
		this (id, true, false, false);
	}

	protected AbstractEntity(Id id, boolean isNew)
	{
		this (id, isNew, false, false);
	}

	protected AbstractEntity(Id id, boolean isNew, boolean isModified)
	{
		this (id, isNew, isModified, false);
	}

	protected AbstractEntity(Id id, boolean isNew, boolean isModified, boolean isRemoved)
	{
		this.id = id;

		this.isNew = isNew;
		this.isModified = isModified;
		this.isRemoved = isRemoved;
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public Id getId()
	{
		return id;
	}

	public void setId(Id id)
	{
		this.id = id;
		setModified();
	}

// Helper Methods ------------------------------------------------------------------------------------------------------

	public boolean isNew()
	{
		return isNew;
	}

	public void setNew()
	{
		isNew = true;
	}

	public void resetNew()
	{
		isNew = false;
	}

	public boolean isModified()
	{
		return isModified;
	}

	public void setModified()
	{
		isModified = true;
	}

	public void resetModified()
	{
		isModified = false;
	}

	public boolean isRemoved()
	{
		return isRemoved;
	}

	public void setRemoved()
	{
		isRemoved = true;
	}

	public void resetRemoved()
	{
		isRemoved = false;
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public boolean equals(Object o)
	{
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		final AbstractEntity that = (AbstractEntity) o;
		return !(id != null ? !id.equals(that.id) : that.id != null);
	}

	@Override
	public int hashCode()
	{
		return id != null ? id.hashCode() : 0;
	}

	@Override
	public String toString()
	{
		return mainAttributesToString()
		     + '}';
	}

	protected String mainAttributesToString()
	{
		return getClass().getSimpleName()
		     + "{id=" + id
		     + ", isNew=" + isNew
		     + ", isModified=" + isModified
		     + ", isRemoved=" + isRemoved;
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private Id id;
	private boolean isNew;
	private boolean isModified;
	private boolean isRemoved;
}
