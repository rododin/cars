/*
 * CountryDao.java
 */

package org.rododin.cars.backend.daos.impl.dictionaries;

import java.util.Collection;
import javax.persistence.EntityManager;

import org.rododin.cars.backend.BackEnd;
import org.rododin.cars.backend.daos.DaoException;
import org.rododin.cars.backend.daos.impl.AbstractCachedDao;
import org.rododin.cars.backend.entities.impl.dictionaries.CountryEntity;

/**
 * Description.
 * @author Rod Odin
 */
public class CountryDao
	extends AbstractCachedDao<Integer, CountryEntity>
{
// Constructors --------------------------------------------------------------------------------------------------------

	public CountryDao(EntityManager entityManager)
	{
		super(entityManager);
	}

// Initialization & Termination ----------------------------------------------------------------------------------------

	@Override
	public void init(BackEnd backEnd)
		throws DaoException
	{
		doInit(CountryEntity.class, "select c from CountryEntity as c order by c.isoCode2");

		linkCountriesToLanguages(backEnd);
	}

// DAO Operations ------------------------------------------------------------------------------------------------------

// Internal Logic ------------------------------------------------------------------------------------------------------

	private void linkCountriesToLanguages(BackEnd backEnd)
	{
		final Collection<CountryEntity> countries = getAll();

		countries.forEach(country ->
		{
			country.updateDefaultLanguage();
			country.updateLanguages();
			country.resetModified();
		});
	}
}
