/*
 * AbstractSynchronizedEntity.java
 */

package org.rododin.cars.backend.entities.impl;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class AbstractSynchronizedEntity <Id>
	extends AbstractEntity <Id>
{
// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractSynchronizedEntity()
	{
	}

	protected AbstractSynchronizedEntity(Id id)
	{
		super(id);
	}

	protected AbstractSynchronizedEntity(Id id, boolean isNew)
	{
		super(id, isNew);
	}

	protected AbstractSynchronizedEntity(Id id, boolean isNew, boolean isModified)
	{
		super(id, isNew, isModified);
	}

	protected AbstractSynchronizedEntity(Id id, boolean isNew, boolean isModified, boolean isRemoved)
	{
		super(id, isNew, isModified, isRemoved);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public synchronized void setId(Id id)
	{
		super.setId(id);
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public synchronized boolean equals(Object o)
	{
		return super.equals(o);
	}

	@Override
	public synchronized int hashCode()
	{
		return super.hashCode();
	}

	@Override
	public synchronized String toString()
	{
		return super.toString();
	}
}
