/*
 * AbstractCachedDao.java
 */

package org.rododin.cars.backend.daos.impl;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.persistence.EntityManager;

import org.rododin.cars.backend.daos.CachedDao;
import org.rododin.cars.backend.daos.DaoException;
import org.rododin.cars.backend.entities.Entity;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class AbstractCachedDao
	< Id
	, E extends Entity<Id>
	>
	extends    AbstractDao <Id, E>
	implements CachedDao   <Id, E>
{
// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractCachedDao(EntityManager entityManager)
	{
		super(entityManager);
	}

// Initialization & Termination ----------------------------------------------------------------------------------------

	protected void doInit(Class<E> entityClass, String query)
		throws DaoException
	{
		getEntityManager().clear();
		cache.clear();
		final List<E> entities = doGetMany(entityClass, query);
		if (entities != null)
		{
			for (E entity : entities)
				cache.put(entity.getId(), entity);
		}
	}

// DAO Operations ------------------------------------------------------------------------------------------------------

	@Override
	public Collection<E> getAll()
	{
		return cache.values();
	}

	@Override
	public E get(Id id)
		throws DaoException, UnsupportedOperationException
	{
		return cache.get(id);
	}

	@Override
	public void serialize(E entity)
		throws DaoException
	{
		super.serialize(entity);
		if (!entity.isRemoved())
			cache.put(entity.getId(), entity);
		else
			cache.remove(entity.getId());
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final ConcurrentMap<Id, E> cache = new ConcurrentHashMap<>();

}
