/*
 * DaoException.java
 */

package org.rododin.cars.backend.daos;

/**
 * A common unchecked exception used to notify about DB/storage access issues or other issues happening
 * on data read/write via misc. <code>{@link Dao}</code> methods.
 *
 * @author Rod Odin
 */
public class DaoException
	extends RuntimeException
{
	public DaoException()
	{
	}

	public DaoException(String message)
	{
		super(message);
	}

	public DaoException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public DaoException(Throwable cause)
	{
		super(cause);
	}

	public DaoException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
