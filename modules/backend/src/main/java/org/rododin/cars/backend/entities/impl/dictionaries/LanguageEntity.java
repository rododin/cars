/*
 * LanguageEntity.java
 */

package org.rododin.cars.backend.entities.impl.dictionaries;

import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;
import org.rododin.cars.backend.entities.impl.AbstractEntity;

/**
 * Description.
 * @author Rod Odin
 */
@Entity
@Table(name = "languages")
public class LanguageEntity
	extends AbstractEntity<Integer>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final LanguageEntity DEFAULT_LANGUAGE = new LanguageEntity("ru", "rus", "Russian", "Русский");

// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public LanguageEntity()
	{
	}

	public LanguageEntity(String isoCode2, String isoCode3, String nameEn, String nameOrig)
	{
		setIsoCode2(isoCode2);
		setIsoCode3(isoCode3);
		setNameEn(nameEn);
		setNameOrig(nameOrig);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "language_id", nullable = false, unique = true)
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId()
	{
		return super.getId();
	}

	@Override
	public void setId(Integer id)
	{
		super.setId(id);
	}

	@NaturalId
	@Column(name = "iso_code_2", columnDefinition = "char", nullable = false, unique = true)
	public String getIsoCode2()
	{
		return isoCode2;
	}

	public void setIsoCode2(String isoCode2)
	{
		this.isoCode2 = isoCode2;
		setModified();
	}

	@Column(name = "iso_code_3", columnDefinition = "char", nullable = false, unique = true)
	public String getIsoCode3()
	{
		return isoCode3;
	}

	public void setIsoCode3(String isoCode3)
	{
		this.isoCode3 = isoCode3;
		setModified();
	}

	@Column(name = "name_en", nullable = false)
	public String getNameEn()
	{
		return nameEn;
	}

	public void setNameEn(String nameEn)
	{
		this.nameEn = nameEn;
		setModified();
	}

	@Column(name = "name_orig", nullable = false)
	public String getNameOrig()
	{
		return nameOrig;
	}

	public void setNameOrig(String nameOrig)
	{
		this.nameOrig = nameOrig;
		setModified();
	}

	@OneToMany(mappedBy = "language", cascade = CascadeType.ALL)
	public List<CountryLanguageEntity> getCountryLanguages()
	{
		return countryLanguages;
	}

	public void setCountryLanguages(List<CountryLanguageEntity> countryLanguages)
	{
		this.countryLanguages = countryLanguages;
		setModified();
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public String toString()
	{
		final List<CountryLanguageEntity> countryLanguages = this.countryLanguages;

		return mainAttributesToString()
		     + ", isoCode2="          + isoCode2
		     + ", isoCode3="          + isoCode3
		     + ", nameEn="            + nameEn
		     + ", nameOrig="          + nameOrig
		     + ", languageCountries=" + (countryLanguages != null ? countryLanguages.stream().map(cl -> cl.getCountry().getIsoCode2() + (Boolean.TRUE.equals(cl.getDefault()) ? "-default" : "") + (Boolean.TRUE.equals(cl.getOfficial()) ? "-official" : "")).collect(Collectors.toList()) : null)
		     + '}';
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private static final long serialVersionUID = 8695531144215089617L;

	private String isoCode2;
	private String isoCode3;
	private String nameEn;
	private String nameOrig;
	private List<CountryLanguageEntity> countryLanguages;
}
