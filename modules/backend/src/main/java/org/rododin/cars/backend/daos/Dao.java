/*
 * Dao.java
 */

package org.rododin.cars.backend.daos;

import javax.persistence.EntityManager;

import org.rododin.cars.backend.BackEnd;
import org.rododin.cars.backend.entities.Entity;

/**
 * Introduces an abstract DAO controller.
 *
 * @author Rod Odin
 */
public interface Dao
	< Id
	, E extends Entity<Id>
	>
{
	EntityManager getEntityManager();

	default void init(BackEnd backEnd) throws DaoException
	{
	}

	default void destroy() throws DaoException
	{
	}

	/**
	 * Queries the DB/storage in order to get an <code>{@link Entity}</code> by the given <code>id</code>.
	 * @param id non-<code>null</code> entity ID
	 * @return the requested entity or <code>null</code> if no entity found
	 * @throws DaoException thrown on DB/storage access error
	 * @throws UnsupportedOperationException thrown if the operation is not implemented (by default)
	 */
	default E get(Id id) throws DaoException, UnsupportedOperationException
	{
		throw new UnsupportedOperationException();
	}

	/**
	 * Adds/inserts, or updates, or removes the given <code>entity</code> in(to)/from DB/storage.
	 * A <code>{@link Entity#isNew() new}</code> entity will be added/inserted,
	 * a <code>{@link Entity#isModified()} () modified}</code> one will be updated,
	 * and a <code>{@link Entity#isRemoved()} () removed}</code> one will be removed.
	 * @param entity non-<code>null</code> <code>{@link Entity}</code> instance
	 * @throws DaoException thrown on DB/storage access error, incl. constrain violation and other issues
	 * @throws UnsupportedOperationException thrown if the operation is not implemented (by default)
	 */
	default void serialize(E entity) throws DaoException, UnsupportedOperationException
	{
		throw new UnsupportedOperationException();
	}
}
