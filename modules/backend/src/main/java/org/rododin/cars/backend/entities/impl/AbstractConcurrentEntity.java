/*
 * AbstractEntityImpl.java
 */

package org.rododin.cars.backend.entities.impl;

import org.rododin.cars.generic.utils.locks.ReentrantLockHelper.LockType;
import org.rododin.cars.generic.utils.locks.SingleReentrantLockHelper;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class AbstractConcurrentEntity <Id>
	extends AbstractEntity <Id>
{
// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractConcurrentEntity()
	{
	}

	protected AbstractConcurrentEntity(Id id)
	{
		super(id);
	}

	protected AbstractConcurrentEntity(Id id, boolean isNew)
	{
		super(id, isNew);
	}

	protected AbstractConcurrentEntity(Id id, boolean isNew, boolean isModified)
	{
		super(id, isNew, isModified);
	}

	protected AbstractConcurrentEntity(Id id, boolean isNew, boolean isModified, boolean isRemoved)
	{
		super(id, isNew, isModified, isRemoved);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setId(Id id)
	{
		lock.run(LockType.WRITE, () -> super.setId(id));
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public boolean equals(Object o)
	{
		return lock.call(LockType.WRITE, () -> super.equals(o));
	}

	@Override
	public int hashCode()
	{
		return lock.call(LockType.WRITE, super::hashCode);
	}

	@Override
	public String toString()
	{
		return lock.call(LockType.WRITE, super::toString);
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final SingleReentrantLockHelper lock = new SingleReentrantLockHelper();
}
