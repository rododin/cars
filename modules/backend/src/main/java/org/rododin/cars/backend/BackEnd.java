/*
 * BackEnd.java
 */

package org.rododin.cars.backend;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.rododin.cars.backend.daos.Dao;
import org.rododin.cars.backend.daos.impl.dictionaries.CountryDao;
import org.rododin.cars.backend.daos.impl.dictionaries.LanguageDao;
import org.rododin.cars.backend.entities.Entity;
import org.rododin.cars.generic.log.Log;
import org.rododin.cars.generic.utils.LazyInitializer;

/**
 * The Back-End manager, behaves as a singleton.
 * Use {@link #get()} to get the singleton instance.
 * Formally the class is extensible, but there're no plans to extend it some way.
 *
 * @author Rod Odin
 */
public class BackEnd
{
// The Main Routine ----------------------------------------------------------------------------------------------------

	/** It's totally redundant here, keeping it for misc dev-time usage only. */
	public static void main(String[] args)
	{
		Log.infoForced("Cars - BackEnd");
	}

// Constructors --------------------------------------------------------------------------------------------------------

	protected BackEnd()
	{
		entityManagerFactory = Persistence.createEntityManagerFactory(getClass().getPackage().getName() + ".jpa");
		defaultEntityManager = entityManagerFactory.createEntityManager();

		initDao(LanguageDao::new);
		initDao(CountryDao ::new);
	}

// Initialization/Finalization -----------------------------------------------------------------------------------------

	protected <Id, E extends Entity<Id>, DAO extends Dao<Id, E>> void initDao(Function<EntityManager, DAO> constructor)
	{
		final DAO dao = constructor.apply(defaultEntityManager);
		daos.put(dao.getClass().getName(), new LazyInitializer<>(() -> {
			dao.init(this);
			return dao;
		}));
	}

	protected void finalize() throws Throwable
	{
		daos.values().forEach(li -> {
			final Dao<?, ?> dao = li.getIfInitialized();
			if (dao != null)
				dao.destroy();
		});

		defaultEntityManager.close();
		entityManagerFactory.close();
		super.finalize();
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public static BackEnd get()
	{
		return SINGLETON.get();
	}

	@SuppressWarnings("unchecked")
	public <Id, E extends Entity<Id>, DAO extends Dao<Id, E>> DAO getDao(Class<DAO> daoClass)
	{
		return (DAO)daos.get(daoClass.getName()).get();
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private static final LazyInitializer<BackEnd> SINGLETON = new LazyInitializer<>(BackEnd::new);

	private final EntityManagerFactory entityManagerFactory;
	private final EntityManager defaultEntityManager;

	private final Map<String, LazyInitializer<? extends Dao<?, ?>>> daos = new HashMap<>();
}
