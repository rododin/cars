/*
 * AbstractDaoImpl.java
 */

package org.rododin.cars.backend.daos.impl;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Supplier;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

import org.rododin.cars.backend.daos.Dao;
import org.rododin.cars.backend.daos.DaoException;
import org.rododin.cars.backend.entities.Entity;
import org.rododin.cars.generic.log.Log;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class AbstractDao
	< Id
	, E extends Entity<Id>
	>
	implements Dao <Id, E>
{
// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractDao(EntityManager entityManager)
	{
		this.entityManager = entityManager;
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public EntityManager getEntityManager()
	{
		return entityManager;
	}

// DAO Operations ------------------------------------------------------------------------------------------------------

	protected E doGetOne(Class<E> entityClass, String query)
		throws DaoException
	{
		final E entity = executeInTransaction( () -> entityManager.createQuery(query, entityClass).getSingleResult()
		                                     , () -> "doGetOne(" + (entityClass != null ? entityClass.getSimpleName() : null) + ", \"" + query + "\")");
		if (entity != null)
		{
			entity.resetNew();
			entity.resetModified();
		}
		return entity;
	}

	protected List<E> doGetMany(Class<E> entityClass, String query)
		throws DaoException
	{
		final List<E> entities = executeInTransaction( () -> entityManager.createQuery(query, entityClass).getResultList()
		                                             , () -> "doGetMany(" + (entityClass != null ? entityClass.getSimpleName() : null) + ", \"" + query + "\")");
		if (entities != null)
		{
			for (E entity : entities)
			{
				entity.resetNew();
				entity.resetModified();
			}
		}
		return entities;
	}

	@Override
	public void serialize(E entity)
		throws DaoException
	{
		if (entity == null)
			throw new DaoException("Entity is null: class=" + getClass().getSimpleName());

		final String info = "serialize(" + entity + ")";

		if (entity.isNew())
			executeInTransaction(() -> entityManager.persist(entity), () -> info);
		else if (entity.isModified())
			executeInTransaction(() -> entityManager.merge  (entity), () -> info);
		else if (entity.isRemoved())
			executeInTransaction(() -> entityManager.remove (entity), () -> info);
		else
			Log.warn("Nothing to do: " + info);

		entity.resetNew();
		entity.resetModified();
	}

	protected void executeInTransaction(Runnable task, Supplier<String> errorDetailsSupplier)
		throws DaoException
	{
		executeInTransaction(() -> { task.run(); return null; }, errorDetailsSupplier);
	}

	protected <Result> Result executeInTransaction(Callable<Result> task, Supplier<String> errorDetailsSupplier)
		throws DaoException
	{
		EntityTransaction transaction = null;
		try
		{
			final EntityManager entityManager = getEntityManager();
			transaction = entityManager.getTransaction();
			transaction.begin();
			final Result result = task.call();
			transaction.commit();
			return result;
		}
		catch (NoResultException x)
		{
			if (transaction != null)
				transaction.rollback();
			return null;
		}
		catch (Exception x)
		{
			if (transaction != null)
				transaction.rollback();
			throw new DaoException( "Data read/write error: class=" + getClass().getSimpleName()
			                      + (errorDetailsSupplier != null ? ", details=" + errorDetailsSupplier.get() : "")
			                      , x);
		}
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final EntityManager entityManager;
}
