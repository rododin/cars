/*
 * CountryLanguageEntity.java
 */

package org.rododin.cars.backend.entities.impl.dictionaries;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.rododin.cars.backend.entities.impl.AbstractEntity;

/**
 * Description.
 * @author Rod Odin
 */
@Entity
@Table(name = "country_languages")
public class CountryLanguageEntity
	extends AbstractEntity<Integer>
{
// Constants -----------------------------------------------------------------------------------------------------------

// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public CountryLanguageEntity()
	{
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "country_language_id", nullable = false, unique = true)
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId()
	{
		return super.getId();
	}


	@Override
	public void setId(Integer id)
	{
		super.setId(id);
	}

	@ManyToOne
	@JoinColumn(name = "country_id")
	public CountryEntity getCountry()
	{
		return country;
	}

	public void setCountry(CountryEntity country)
	{
		this.country = country;
		setModified();
	}

	@ManyToOne
	@JoinColumn(name = "language_id")
	public LanguageEntity getLanguage()
	{
		return language;
	}

	public void setLanguage(LanguageEntity language)
	{
		this.language = language;
		setModified();
	}

	@Column(name = "is_official", nullable = false)
	public Boolean getOfficial()
	{
		return isOfficial;
	}

	public void setOfficial(Boolean isOfficial)
	{
		this.isOfficial = isOfficial;
		setModified();
	}

	@Column(name = "is_default", nullable = false)
	public Boolean getDefault()
	{
		return isDefault;
	}

	public void setDefault(Boolean isDefault)
	{
		this.isDefault = isDefault;
		setModified();
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

		@Override
		public String toString()
		{
			final CountryEntity country = this.country;
			final LanguageEntity language = this.language;
			return mainAttributesToString()
			       + ", country=" + (country != null ? country.getIsoCode2() : null)
			       + ", language=" + (language != null ? language.getIsoCode2() : null)
			       + ", isOfficial=" + isOfficial
			       + ", nameOrig=" + isDefault
			       + '}';
		}

// Attributes ----------------------------------------------------------------------------------------------------------

	private CountryEntity country;
	private LanguageEntity language;
	private Boolean isOfficial;
	private Boolean isDefault;
}
