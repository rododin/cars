/*
 * Entity.java
 */

package org.rododin.cars.backend.entities;

/**
 * Description.
 * @author Rod Odin
 */
public interface Entity <Id>
{
	Id getId();

	boolean isNew();
	void setNew();
	void resetNew();

	boolean isModified();
	void setModified();
	void resetModified();

	boolean isRemoved();
	void setRemoved();
	void resetRemoved();
}
