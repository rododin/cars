/*
 * CountryEntity.java
 */

package org.rododin.cars.backend.entities.impl.dictionaries;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;
import org.rododin.cars.backend.entities.impl.AbstractEntity;

/**
 * Description.
 * @author Rod Odin
 */
@Entity
@Table(name = "countries")
public class CountryEntity
	extends AbstractEntity<Integer>
{
	// Constants -----------------------------------------------------------------------------------------------------------

	public static final CountryEntity DEFAULT_COUNTRY = new CountryEntity("RU", "RUS", "Russia", "Россия", LanguageEntity.DEFAULT_LANGUAGE);

	// Constructors --------------------------------------------------------------------------------------------------------

	/**
	 * Required for Hibernate.
	 */
	public CountryEntity()
	{
	}

	public CountryEntity(String isoCode2, String isoCode3, String nameEn, String nameOrig, LanguageEntity defaultLanguage)
	{
		setIsoCode2(isoCode2);
		setIsoCode3(isoCode3);
		setNameEn(nameEn);
		setNameOrig(nameOrig);
		setDefaultLanguage(defaultLanguage);
	}

	// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Id
	@Column(name = "country_id", nullable = false, unique = true)
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment", strategy = "increment")
	public Integer getId()
	{
		return super.getId();
	}

	@Override
	public void setId(Integer id)
	{
		super.setId(id);
	}

	@NaturalId
	@Column(name = "iso_code_2", columnDefinition = "char", nullable = false, unique = true)
	public String getIsoCode2()
	{
		return isoCode2;
	}

	public void setIsoCode2(String isoCode2)
	{
		this.isoCode2 = isoCode2;
		setModified();
	}

	@Column(name = "iso_code_3", columnDefinition = "char", nullable = false, unique = true)
	public String getIsoCode3()
	{
		return isoCode3;
	}

	public void setIsoCode3(String isoCode3)
	{
		this.isoCode3 = isoCode3;
		setModified();
	}

	@Column(name = "name_en", nullable = false)
	public String getNameEn()
	{
		return nameEn;
	}

	public void setNameEn(String nameEn)
	{
		this.nameEn = nameEn;
		setModified();
	}

	@Column(name = "name_orig", nullable = false)
	public String getNameOrig()
	{
		return nameOrig;
	}

	public void setNameOrig(String nameOrig)
	{
		this.nameOrig = nameOrig;
		setModified();
	}

	@Transient
	public LanguageEntity getDefaultLanguage()
	{
		return defaultLanguage;
	}

	public void updateDefaultLanguage()
	{
		final List<CountryLanguageEntity> countryLanguages = this.countryLanguages;
		final Optional<CountryLanguageEntity> clOptional = countryLanguages != null ? countryLanguages.stream().filter(cl -> Boolean.TRUE.equals(cl.getDefault())).findFirst() : null;
		setDefaultLanguage(clOptional != null && clOptional.isPresent() ? clOptional.get().getLanguage() : null);
	}

	public void setDefaultLanguage(LanguageEntity defaultLanguage)
	{
		this.defaultLanguage = defaultLanguage;
		setModified();
	}

	@Transient
	public Collection<LanguageEntity> getLanguages()
	{
		return languages;
	}

	public void updateLanguages()
	{
		final List<CountryLanguageEntity> countryLanguages = this.countryLanguages;
		setLanguages(countryLanguages != null ? countryLanguages.stream().map(CountryLanguageEntity::getLanguage).collect(Collectors.toCollection(LinkedHashSet::new)) : null);
	}

	public void setLanguages(Collection<LanguageEntity> languages)
	{
		if (languages == null)
			this.languages = null;
		else if (languages instanceof Set)
			this.languages = (Set<LanguageEntity>)languages;
		else
			this.languages = new LinkedHashSet<>(languages);
		setModified();
	}

	@OneToMany(mappedBy = "country", cascade = CascadeType.ALL)
	public List<CountryLanguageEntity> getCountryLanguages()
	{
		return countryLanguages;
	}

	public void setCountryLanguages(List<CountryLanguageEntity> countryLanguages)
	{
		this.countryLanguages = countryLanguages;
		setModified();
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public String toString()
	{
		final List<CountryLanguageEntity> countryLanguages = this.countryLanguages;
		final LanguageEntity defaultLanguage = getDefaultLanguage();
		final Collection<LanguageEntity> languages = getLanguages();

		return mainAttributesToString()
		     + ", isoCode2="         + isoCode2
		     + ", isoCode3="         + isoCode3
		     + ", nameEn="           + nameEn
		     + ", nameOrig="         + nameOrig
		     + ", defaultLanguage="  + (defaultLanguage != null ? defaultLanguage.getIsoCode2() : null)
		     + ", languages="        + (languages != null ? languages.stream().map(LanguageEntity::getIsoCode2).collect(Collectors.toList()) : null)
		     + ", countryLanguages=" + (countryLanguages != null ? countryLanguages.stream().map(cl -> cl.getLanguage().getIsoCode2() + (Boolean.TRUE.equals(cl.getDefault()) ? "-default" : "") + (Boolean.TRUE.equals(cl.getOfficial()) ? "-official" : "")).collect(Collectors.toList()) : null)
		     + '}';
	}

	// Attributes ----------------------------------------------------------------------------------------------------------

	private static final long serialVersionUID = -2395975529242590923L;

	private String isoCode2;
	private String isoCode3;
	private String nameEn;
	private String nameOrig;
	private LanguageEntity defaultLanguage;
	private Set<LanguageEntity> languages;
	private List<CountryLanguageEntity> countryLanguages;
}
