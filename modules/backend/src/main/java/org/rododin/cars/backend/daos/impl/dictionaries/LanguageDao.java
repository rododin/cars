/*
 * LanguageDao.java
 */

package org.rododin.cars.backend.daos.impl.dictionaries;

import javax.persistence.EntityManager;

import org.rododin.cars.backend.BackEnd;
import org.rododin.cars.backend.daos.DaoException;
import org.rododin.cars.backend.daos.impl.AbstractCachedDao;
import org.rododin.cars.backend.entities.impl.dictionaries.LanguageEntity;

/**
 * Description.
 * @author Rod Odin
 */
public class LanguageDao
	extends AbstractCachedDao<Integer, LanguageEntity>
{
// Constructors --------------------------------------------------------------------------------------------------------

	public LanguageDao(EntityManager entityManager)
	{
		super(entityManager);
	}

// Initialization & Termination ----------------------------------------------------------------------------------------

	@Override
	public void init(BackEnd backEnd)
		throws DaoException
	{
		doInit(LanguageEntity.class, "select l from LanguageEntity as l order by l.isoCode2");
	}

// DAO Operations ------------------------------------------------------------------------------------------------------

}
