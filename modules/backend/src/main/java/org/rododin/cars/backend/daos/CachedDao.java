/*
 * CachedDao.java
 */

package org.rododin.cars.backend.daos;

import java.util.Collection;

import org.rododin.cars.backend.entities.Entity;

/**
 * Description.
 * @author Rod Odin
 */
public interface CachedDao
	< Id
	, E extends Entity<Id>
	>
	extends Dao<Id, E>
{
	default Collection<E> getAll()
	{
		throw new UnsupportedOperationException();
	}
}
