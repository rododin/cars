# Database Selection ---------------------------------------------------------------------------------------------------

use cars;

# Table Creation -------------------------------------------------------------------------------------------------------

drop table if exists `brands`;
drop table if exists `brand_types`;

create table if not exists `brand_types`
(
	`brand_type_id` int    ( 10) unsigned not null auto_increment,
	`name`          varchar( 16)          not null               ,
	primary key               (`brand_type_id`),
	        index   `ix_name` (`name`      asc)
) engine = InnoDB;

create table if not exists `brands`
(
	`brand_id`             int    ( 10) unsigned not null auto_increment,
	`trans_brand_id`       int    ( 10) unsigned          default null  , -- points to transition target (i.e. points to the new brand, if the current one has been renamed or reorganized, or re-owned, etc.)
	`brand_type_id`        int    ( 10) unsigned not null               , -- specifies the current brand type (e.g. 'Car', or 'Engine', or 'Transmission', etc.)
	`corporation_id`       int    ( 10) unsigned not null               , -- points to the owning corporation
	`country_id`           int    ( 10) unsigned not null               ,
	`name_short_en`        varchar( 16)          not null               ,
	`name_full_en`         varchar(128)          not null               ,
	`name_short_orig`      varchar( 16)          not null               ,
	`name_full_orig`       varchar(128)          not null               ,
	`foundation_date`      date                           default null  , -- also may mean the owning start date
	`termination_date`     date                           default null  , -- also may mean the owning termination date
	`website`              varchar(256)                   default null  ,
	`wikipage`             varchar(512)                   default null  ,
	primary key                             (`brand_id`                ),
	        index `fk_br_trans_brand_id`    (`trans_brand_id`       asc),
	        index `fk_br_brand_type_id`     (`brand_type_id`        asc),
	        index `fk_br_corporation_id`    (`corporation_id`       asc),
	        index `fk_br_country_id`        (`country_id`           asc),
	        index `ix_name_short_en`        (`name_short_en`        asc),
	        index `ix_name_full_en`         (`name_full_en`         asc),
	        index `ix_name_short_orig`      (`name_short_orig`      asc),
	        index `ix_name_full_orig`       (`name_full_orig`       asc),
	        index `ix_foundation_date`      (`foundation_date`      asc),
	        index `ix_termination_date`     (`termination_date`     asc),
	   constraint `fk_br_trans_brand_id` foreign key (`trans_brand_id` ) references `brands`       (`brand_id`       ),
	   constraint `fk_br_brand_type_id`  foreign key (`brand_type_id`  ) references `brand_types`  (`brand_type_id`  ),
	   constraint `fk_br_corporation_id` foreign key (`corporation_id` ) references `corporations` (`corporation_id` ),
	   constraint `fk_br_country_id`     foreign key (`country_id`     ) references `countries`    (`country_id`     )
) engine = InnoDB;

# Data Insertion - Brand Types -----------------------------------------------------------------------------------------

insert into `brand_types` (name) values ('Vehicle'     );
insert into `brand_types` (name) values ('Engine'      );
insert into `brand_types` (name) values ('Transmission');
insert into `brand_types` (name) values ('Suspension'  );
insert into `brand_types` (name) values ('Tyre'        );
insert into `brand_types` (name) values ('Acoustics'   );
insert into `brand_types` (name) values ('TuningStudio');
#...

# Data Insertion - Brands ----------------------------------------------------------------------------------------------

# TODO: Fill some data...
