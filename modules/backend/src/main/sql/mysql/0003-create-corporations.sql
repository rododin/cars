# Database Selection ---------------------------------------------------------------------------------------------------

use cars;

# Table Creation -------------------------------------------------------------------------------------------------------

drop table if exists `corporations`;

create table if not exists `corporations`
(
	`corporation_id`       int    ( 10) unsigned not null auto_increment,
	`owner_corporation_id` int    ( 10) unsigned          default null  , -- points to the owning corporation if there is any
	`trans_corporation_id` int    ( 10) unsigned          default null  , -- points to transition target (i.e. points to the new corporation, if the current one has been renamed or reorganized, or re-owned, etc.)
	`country_id`           int    ( 10) unsigned not null               ,
	`name_short_en`        varchar( 16)          not null               ,
	`name_full_en`         varchar(128)          not null               ,
	`name_short_orig`      varchar( 16)          not null               ,
	`name_full_orig`       varchar(128)          not null               ,
	`foundation_date`      date                           default null  ,
	`termination_date`     date                           default null  ,
	`website`              varchar(256)                   default null  ,
	`wikipage`             varchar(512)                   default null  ,
	`comments`             varchar(512)                   default null  ,
	primary key                                (`corporation_id`          ),
	        index `fk_co_owner_corporation_id` (`owner_corporation_id` asc),
	        index `fk_co_trans_corporation_id` (`trans_corporation_id` asc),
	        index `fk_co_country_id`           (`country_id`           asc),
	        index `ix_name_short_en`           (`name_short_en`        asc),
	        index `ix_name_full_en`            (`name_full_en`         asc),
	        index `ix_name_short_orig`         (`name_short_orig`      asc),
	        index `ix_name_full_orig`          (`name_full_orig`       asc),
	        index `ix_foundation_date`         (`foundation_date`      asc),
	        index `ix_termination_date`        (`termination_date`     asc),
	   constraint `fk_co_owner_corporation_id` foreign key (`owner_corporation_id`) references `corporations` (`corporation_id`),
	   constraint `fk_co_trans_corporation_id` foreign key (`trans_corporation_id`) references `corporations` (`corporation_id`),
	   constraint `fk_co_country_id`           foreign key (`country_id`          ) references `countries`    (`country_id`    )
) engine = InnoDB;

# Data Insertion - Corporations ----------------------------------------------------------------------------------------

# TODO: Fill some data...
