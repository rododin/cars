# Database Selection ---------------------------------------------------------------------------------------------------

use cars;

# Table Creation -------------------------------------------------------------------------------------------------------

drop table if exists `country_languages`;
drop table if exists `countries`;
drop table if exists `languages`;

create table if not exists `languages`
(
	`language_id`    int    ( 10) unsigned not null auto_increment,
	`iso_code_2`     char   (  2)          not null,
	`iso_code_3`     char   (  3)          not null,
	`name_en`        varchar( 64)          not null,
	`name_orig`      varchar( 64)          not null,
	primary key                   (`language_id`   ),
	unique  index `ui_iso_code_2` (`iso_code_2` asc),
	unique  index `ui_iso_code_3` (`iso_code_3` asc),
	        index `ix_name_en`    (`name_en`    asc),
	        index `ix_name_orig`  (`name_orig`  asc)
) engine = InnoDB;

create table if not exists `countries`
(
	`country_id`     int    ( 10) unsigned not null auto_increment,
	`iso_code_2`     char   (  2)          not null,
	`iso_code_3`     char   (  3)          not null,
	`name_en`        varchar(128)          not null,
	`name_orig`      varchar(128)          not null,
	primary key                   (`country_id`    ),
	unique  index `ui_iso_code_2` (`iso_code_2` asc),
	unique  index `ui_iso_code_3` (`iso_code_3` asc),
	        index `ix_name_en`    (`name_en`    asc),
	        index `ix_name_orig`  (`name_orig`  asc)
) engine = InnoDB;

create table if not exists `country_languages`
(
	`country_language_id` int    ( 10) unsigned not null auto_increment,
	`country_id`          int    ( 10) unsigned not null,
	`language_id`         int    ( 10) unsigned not null,
	`is_official`         tinyint(  1)          not null default 0,
	`is_default`          tinyint(  1)          not null default 0,
	primary key                       (`country_language_id`),
	        index `fk_cl_country_id`  (`country_id`      asc),
	        index `fk_cl_language_id` (`language_id`     asc),
	        index `ix_is_official`    (`is_official`     asc),
	        index `ix_is_default`     (`is_default`     asc),
	   constraint `fk_cl_country_id`  foreign key (`country_id` ) references `countries` (`country_id` ),
	   constraint `fk_cl_language_id` foreign key (`language_id`) references `languages` (`language_id`)
) engine = InnoDB;

# Data Insertion - Languages -------------------------------------------------------------------------------------------

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('ru'      , 'rus'     , 'Russian'   , 'Русский'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('en'      , 'eng'     , 'English'   , 'English'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('de'      , 'deu'     , 'German'    , 'Deutsch'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('fr'      , 'fra'     , 'French'    , 'Français'   );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('nl'      , 'nld'     , 'Dutch'    , 'Nederlands'  );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('sv'      , 'swe'     , 'Swedish'   , 'svenska'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('it'      , 'ita'     , 'Italian'   , 'Italiano'   );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('el'      , 'ell'     , 'Greek'     , 'ελληνικά'   );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('zh'      , 'zho'     , 'Chinese'   , '中文'       );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('ja'      , 'jpn'     , 'Japanese'  , '日本語'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('ar'      , 'ara'     , 'Arabic'    , 'العربية'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('hi'      , 'hin'     , 'Hindi'     , 'हिन्दी, हिंदी' );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('sa'      , 'san'     , 'Sanskrit'  , 'संस्कृतम्'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('ko'      , 'kor'     , 'Korean'    , '한국어'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('vi'      , 'vie'     , 'Vietnamese', 'Tiếng Việt' );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('es'      , 'spa'     , 'Spanish'   , 'Español'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('pt'      , 'por'     , 'Portuguese', 'Português'  );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('hy'      , 'hye'     , 'Armenian'  , 'հայերէն'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('be'      , 'bel'     , 'Belarusian', 'Беларуская' );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('kk'      , 'kaz'     , 'Kazakh'    , 'қазақ тілі' );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('uz'      , 'uzb'     , 'Uzbek'     , 'Ўзбек'      );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('uk'      , 'ukr'     , 'Ukrainian' , 'Українська' );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('bg'      , 'bul'     , 'Bulgarian'  , 'Български' );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('cs'      , 'ces'     , 'Czech'     , 'čeština'    );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('hu'      , 'hun'     , 'Hungarian' , 'magyar'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('pl'      , 'pol'     , 'Polish'    , 'polszczyzna');

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('ro'      , 'ron'     , 'Romanian'  , 'Română'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('fa'      , 'fas'     , 'Persian'   , 'فارسی'      );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('he'      , 'heb'     , 'Hebrew'    , 'עברית'      );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('yi'      , 'yid'     , 'Yiddish'    , 'אידיש'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('tr'      , 'tur'     , 'Turkish'   , 'Türkçe'     );

insert into `languages`
	(iso_code_2, iso_code_3, name_en     , name_orig    )
values
	('bn'      , 'ben'     , 'Bengali'   , 'বাংলা'      );

# Data Insertion - Countries -------------------------------------------------------------------------------------------

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('RU'      , 'RUS'     , 'Russia'        , 'Россия'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('US'      , 'USA'     , 'United States' , 'United States' );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('GB'      , 'GBR'     , 'United Kingdom', 'United Kingdom');

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('AU'      , 'AUS'     , 'Australia'     , 'Australia'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('DE'      , 'DEU'     , 'Germany'       , 'Deutschland'   );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('AT'      , 'AUT'     , 'Austria'       , 'Österreich'    );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('FR'      , 'FRA'     , 'France'        , 'La France'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig      )
values
	('BE'      , 'BEL'     , 'Belgium'       , 'België'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig      )
values
	('NL'      , 'NLD'     , 'Netherlands'   , 'Nederland'    );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('SE'      , 'SWE'     , 'Sweden'        , 'Sverige'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('IT'      , 'ITA'     , 'Italy'         , 'Italia'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('GR'      , 'GRC'     , 'Greece'        , 'Ελλάδα'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('CN'      , 'CHN'     , 'China'         , '中国'          );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('JP'      , 'JPN'     , 'Japan'         , '日本'          );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('IN'      , 'IND'     , 'India'         , 'भारत'          );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('KR'      , 'KOR'     , 'South Korea'   , '대한민국'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('VN'      , 'VIE'     , 'Vietnam'       , 'Việt Nam'      );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('ES'      , 'ESP'     , 'Spain'         , 'España'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('AR'      , 'ARG'     , 'Argentina'     , 'Argentina'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('MX'      , 'MEX'     , 'Mexico'        , 'México'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('PT'      , 'PRT'     , 'Portugal '     , 'Portuguese'    );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('BR'      , 'BRA'     , 'Brazil'        , 'Brasil'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('AM'      , 'ARM'     , 'Armenia'       , 'Հայաստան'    );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('BY'      , 'BLR'     , 'Belarus'       , 'Белару́сь'      );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('KZ'      , 'KAZ'     , 'Kazakhstan'    , 'Қазақстан'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('UZ'      , 'UZB'     , 'Uzbekistan'    , 'Ўзбекистон'    );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('UA'      , 'UKR'     , 'Ukraine'       , 'Україна'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig      )
values
	('BG'      , 'BUL'     , 'Bulgaria'      , 'България'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('CZ'      , 'CZE'     , 'Czech'         , 'Czechia'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('HU'      , 'HUN'     , 'Hungary'       , 'Magyarország'  );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('PL'      , 'POL'     , 'Poland'        , 'Polska'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('RO'      , 'ROU'     , 'Romania'       , 'România'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('IR'      , 'IRN'     , 'Iran'          , 'ایران'         );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('IL'      , 'ISR'     , 'Israel'        , 'יִשְׂרָאֵל'         );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('TR'      , 'TUR'     , 'Turkey'        , 'Türkiye'       );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig       )
values
	('AO'      , 'ANG'     , 'Angola'        , 'Angola'        );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig      )
values
	('BD'      , 'BAN'     , 'Bangladesh'    , 'বাংলাদেশ'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig      )
values
	('BW'      , 'BOT'     , 'Botswana'      , 'Botswana'     );

insert into `countries`
	(iso_code_2, iso_code_3, name_en         , name_orig      )
values
	('GH'      , 'GHA'     , 'Ghana'         , 'Ghana'        );

# Data Insertion - Country Languages -----------------------------------------------------------------------------------

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='RU'),
		(select language_id from languages where iso_code_2='ru'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='US'),
		(select language_id from languages where iso_code_2='en'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='US'),
		(select language_id from languages where iso_code_2='es'),
		0, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='GB'),
		(select language_id from languages where iso_code_2='en'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='AU'),
		(select language_id from languages where iso_code_2='en'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='DE'),
		(select language_id from languages where iso_code_2='de'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='AT'),
		(select language_id from languages where iso_code_2='de'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='FR'),
		(select language_id from languages where iso_code_2='fr'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BE'),
		(select language_id from languages where iso_code_2='de'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BE'),
		(select language_id from languages where iso_code_2='fr'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BE'),
		(select language_id from languages where iso_code_2='nl'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='NL'),
		(select language_id from languages where iso_code_2='nl'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='SE'),
		(select language_id from languages where iso_code_2='sv'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='IT'),
		(select language_id from languages where iso_code_2='it'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='GR'),
		(select language_id from languages where iso_code_2='el'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='CN'),
		(select language_id from languages where iso_code_2='zh'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='JP'),
		(select language_id from languages where iso_code_2='ja'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='KR'),
		(select language_id from languages where iso_code_2='ko'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='VN'),
		(select language_id from languages where iso_code_2='vi'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='IN'),
		(select language_id from languages where iso_code_2='hi'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='IN'),
		(select language_id from languages where iso_code_2='sa'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='ES'),
		(select language_id from languages where iso_code_2='es'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='AR'),
		(select language_id from languages where iso_code_2='es'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='MX'),
		(select language_id from languages where iso_code_2='es'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='PT'),
		(select language_id from languages where iso_code_2='pt'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BR'),
		(select language_id from languages where iso_code_2='pt'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='AM'),
		(select language_id from languages where iso_code_2='hy'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='AM'),
		(select language_id from languages where iso_code_2='ru'),
		0, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BY'),
		(select language_id from languages where iso_code_2='be'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BY'),
		(select language_id from languages where iso_code_2='ru'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='KZ'),
		(select language_id from languages where iso_code_2='kk'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='KZ'),
		(select language_id from languages where iso_code_2='ru'),
		0, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='UZ'),
		(select language_id from languages where iso_code_2='uz'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='UZ'),
		(select language_id from languages where iso_code_2='ru'),
		0, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='UA'),
		(select language_id from languages where iso_code_2='uk'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='UA'),
		(select language_id from languages where iso_code_2='ru'),
		0, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BG'),
		(select language_id from languages where iso_code_2='bg'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='CZ'),
		(select language_id from languages where iso_code_2='cs'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='HU'),
		(select language_id from languages where iso_code_2='hu'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='PL'),
		(select language_id from languages where iso_code_2='pl'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='RO'),
		(select language_id from languages where iso_code_2='ro'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='IR'),
		(select language_id from languages where iso_code_2='fa'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='IL'),
		(select language_id from languages where iso_code_2='he'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='IL'),
		(select language_id from languages where iso_code_2='yi'),
		1, 0
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='TR'),
		(select language_id from languages where iso_code_2='tr'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='AO'),
		(select language_id from languages where iso_code_2='pt'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BD'),
		(select language_id from languages where iso_code_2='bn'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='BW'),
		(select language_id from languages where iso_code_2='en'),
		1, 1
	);

insert into `country_languages`
	(country_id, language_id, is_official, is_default)
values (
		(select  country_id from countries where iso_code_2='GH'),
		(select language_id from languages where iso_code_2='en'),
		1, 1
	);

# Testing --------------------------------------------------------------------------------------------------------------

select c.iso_code_2, c.iso_code_3, c.name_en, c.name_orig, l.iso_code_2, l.iso_code_3, l.name_en, l.name_orig, cl.is_official, cl.is_default
from countries c, languages l, country_languages cl
where cl.country_id = c.country_id and cl.language_id = l.language_id
order by c.iso_code_2, l.iso_code_2;
