# Database Selection ---------------------------------------------------------------------------------------------------

use cars;

# Table Creation -------------------------------------------------------------------------------------------------------

drop table if exists `manufacturers`;
create table if not exists `manufacturers`
	(
	`manufacturer_id`       int    ( 10) unsigned not null auto_increment,
	`trans_manufacturer_id` int    ( 10) unsigned          default null  , -- points to manufacturer target (i.e. points to the new manufacturer, if the current one has been renamed or reorganized, or re-owned, etc.)
	`corporation_id`        int    ( 10) unsigned          default null  , -- points to the owning corporation
	`country_id`            int    ( 10) unsigned not null               ,
	`name_short_en`         varchar( 16)          not null               ,
	`name_full_en`          varchar(128)          not null               ,
	`name_short_orig`       varchar( 16)          not null               ,
	`name_full_orig`        varchar(128)          not null               ,
	`foundation_date`       date                           default null  , -- also may mean the owning start date
	`termination_date`      date                           default null  , -- also may mean the owning termination date
	`website`               varchar(256)                   default null  ,
	`wikipage`              varchar(512)                   default null  ,
	primary key                                 (`manufacturer_id`          ),
	        index `fk_ma_trans_manufacturer_id` (`trans_manufacturer_id` asc),
	        index `fk_ma_corporation_id`        (`corporation_id`        asc),
	        index `fk_ma_country_id`            (`country_id`            asc),
	        index `ix_name_short_en`            (`name_short_en`         asc),
	        index `ix_name_full_en`             (`name_full_en`          asc),
	        index `ix_name_short_orig`          (`name_short_orig`       asc),
	        index `ix_name_full_orig`           (`name_full_orig`        asc),
	        index `ix_foundation_date`          (`foundation_date`       asc),
	        index `ix_termination_date`         (`termination_date`      asc),
	constraint `fk_ma_trans_manufacturer_id` foreign key (`trans_manufacturer_id` ) references `manufacturers` (`manufacturer_id`),
	constraint `fk_ma_corporation_id`        foreign key (`corporation_id`        ) references `corporations`  (`corporation_id` ),
	constraint `fk_ma_country_id`            foreign key (`country_id`            ) references `countries`     (`country_id`     )
) engine = InnoDB;

# Data Insertion - manufacturers ---------------------------------------------------------------------------------------

# Австралия ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1993-00-00', null, 'Amuza', 'Amuza', 'Amuza', 'Amuza', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '2003-00-00', null, 'Birchfield', 'Birchfield', 'Birchfield', 'Birchfield', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1962-00-00', null, 'Bolwell', 'Bolwell', 'Bolwell', 'Bolwell', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1996-00-00', null, 'Bullet', 'Bullet', 'Bullet', 'Bullet', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1999-00-00', null, 'Carbontech', 'Carbontech', 'Carbontech', 'Carbontech', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '2001-00-00', null, 'Daytona', 'Daytona', 'Daytona', 'Daytona', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '2001-00-00', null, 'Devaux', 'Devaux', 'Devaux', 'Devaux', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1981-00-00', null, 'DRB', 'DRB', 'DRB', 'DRB', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1957-00-00', null, 'Elfin', 'Elfin', 'Elfin', 'Elfin', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1925-00-00', null, 'Ford Australia', 'Ford Australia', 'Ford Australia', 'Ford Australia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1925-00-00', null, 'Ford Australia', 'Ford Australia', 'Ford Australia', 'Ford Australia', 'https://www.ford.com.au/', 'https://en.wikipedia.org/wiki/Ford_Australia');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1856-00-00', null, 'Holden', 'Holden', 'Holden', 'Holden', 'https://www.holden.com.au/', 'https://ru.wikipedia.org/wiki/Holden');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1996-00-00', null, 'Joss', 'Joss', 'Joss', 'Joss', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1980-00-00', null, 'Mitsubishi AU', 'Mitsubishi Motors Australia', 'Mitsubishi AU', 'Mitsubishi Motors Australia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1966-00-00', null, 'Nissan Australia', 'Nissan Australia', 'Nissan Australia', 'Nissan Australia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1952-00-00', null, 'Nota', 'Nota', 'Nota', 'Nota', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1978-00-00', null, 'PRB', 'PRB', 'PRB', 'PRB', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1981-00-00', null, 'Python', 'Python', 'Python', 'Python', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1820-00-00', null, 'Tickford', 'Tickford', 'Tickford', 'Tickford', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1973-00-00', null, 'Volkswagen AU', 'Volkswagen Australia', 'Volkswagen AU', 'Volkswagen Australia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1958-00-00', '1960-00-00', 'Ascort', 'Ascort', 'Ascort', 'Ascort', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1926-00-00', '1987-00-00', 'AMI', 'Australian Motor Industries', 'AMI', 'Australian Motor Industries', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1919-00-00', '1925-00-00', 'Australian Six', 'Australian Six', 'Australian Six', 'Australian Six', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1897-00-00', '1907-00-00', 'Australis', 'Australis', 'Australis', 'Australis', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '2008-00-00', '2013-00-00', 'Blade', 'Blade', 'Blade', 'Blade', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1956-00-00', '1961-00-00', 'Buchanan', 'Buchanan', 'Buchanan', 'Buchanan', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1927-00-00', '1967-00-00', 'Buckle', 'Buckle', 'Buckle', 'Buckle', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1907-00-00', '1913-00-00', 'Caldwell Vale', 'Caldwell Vale', 'Caldwell Vale', 'Caldwell Vale', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1951-00-00', '1980-00-00', 'Chrysler AU', 'Chrysler Australia', 'Chrysler AU', 'Chrysler Australia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '2002-00-00', '2014-00-00', 'FPV', 'Ford Performance Vehicles', 'FPV', 'Ford Performance Vehicles', 'https://www.ford.com.au/', 'https://ru.wikipedia.org/wiki/Ford_Performance_Vehicles');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1986-00-00', '1989-00-00', 'Giocattolo', 'Giocattolo', 'Giocattolo', 'Giocattolo', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1955-00-00', '1969-00-00', 'Goggomobil', 'Goggomobil', 'Goggomobil', 'Goggomobil', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1951-00-00', '1955-00-00', 'Hartnett', 'Hartnett', 'Hartnett', 'Hartnett', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1976-00-00', '1990-00-00', 'Kaditcha', 'Kaditcha', 'Kaditcha', 'Kaditcha', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1973-00-00', '1975-00-00', 'Leyland', 'Leyland', 'Leyland', 'Leyland', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1962-00-00', '1965-00-00', 'Lightburn', 'Lightburn', 'Lightburn', 'Lightburn', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1957-00-00', '1962-00-00', 'Lloyd-Hartnett', 'Lloyd-Hartnett', 'Lloyd-Hartnett', 'Lloyd-Hartnett', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1970-00-00', '1979-00-00', 'Pellandini', 'Pellandini', 'Pellandini', 'Pellandini', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1997-00-00', '1998-00-00', 'Pioneer', 'Pioneer', 'Pioneer', 'Pioneer', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1974-00-00', '1991-00-00', 'Purvis Eureka', 'Purvis Eureka', 'Purvis Eureka', 'Purvis Eureka', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1946-00-00', '1965-00-00', 'Rootes Australia', 'Rootes Australia', 'Rootes Australia', 'Rootes Australia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1931-00-00', '1935-00-00', 'Southern Cross', 'Southern Cross', 'Southern Cross', 'Southern Cross', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AU'), '1971-00-00', '1984-00-00', 'Statesman', 'Statesman', 'Statesman', 'Statesman', null, null);

# Австрия ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1932-00-00', null, 'Achleitner', 'Achleitner', 'Achleitner', 'Achleitner', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '2001-00-00', null, 'Magna Steyr', 'Magna Steyr', 'Magna Steyr', 'Magna Steyr', 'http://www.magna.com/', null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1906-00-00', '1908-00-00', 'Alba', 'Alba', 'Alba', 'Alba', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1913-00-00', '1914-00-00', 'Austro', 'Austro', 'Austro', 'Austro', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1899-00-00', '1934-00-00', 'Austro-Daimler', 'Austro-Daimler', 'Austro-Daimler', 'Austro-Daimler', 'https://www.austrodaimler.at/', 'https://ru.wikipedia.org/wiki/Austro-Daimler');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1907-00-00', '1936-00-00', 'Austro-Fiat', 'Austro-Fiat', 'Austro-Fiat', 'Austro-Fiat', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1920-00-00', '1927-00-00', 'Austro-Rumpler', 'Austro-Rumpler', 'Austro-Rumpler', 'Austro-Rumpler', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1934-00-00', '1948-00-00', 'Austro-Tatra', 'Austro-Tatra', 'Austro-Tatra', 'Austro-Tatra', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1921-00-00', '1928-00-00', 'Avis', 'Avis', 'Avis', 'Avis', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1920-00-00', '1925-00-00', 'Baja', 'Baja', 'Baja', 'Baja', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1900-00-00', '1907-00-00', 'Braun', 'Braun', 'Braun', 'Braun', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1901-00-00', '1903-00-00', 'Celeritas', 'Celeritas', 'Celeritas', 'Celeritas', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1970-00-00', '1986-00-00', 'Custoca', 'Custoca', 'Custoca', 'Custoca', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1948-00-00', '1960-00-00', 'Denzel', 'Denzel', 'Denzel', 'Denzel', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1920-00-00', '1926-00-00', 'ESA', 'ESA', 'ESA', 'ESA', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1952-00-00', '1954-00-00', 'Felber', 'Felber', 'Felber', 'Felber', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1932-00-00', '1938-00-00', 'Gloria', 'Gloria', 'Gloria', 'Gloria', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1932-00-00', '1934-00-00', 'Gloriette', 'Gloriette', 'Gloriette', 'Gloriette', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1907-00-00', '1938-00-00', 'Gräf & Stift', 'Gräf & Stift', 'Gräf & Stift', 'Gräf & Stift', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1921-00-00', '1927-00-00', 'Grofri', 'Grofri', 'Grofri', 'Grofri', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1907-00-00', '1963-00-00', 'H&C', 'Hoffmann & Czerny', 'H&C', 'Hoffmann & Czerny', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1964-00-00', '1964-00-00', 'Jamos', 'Jamos', 'Jamos', 'Jamos', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1900-00-00', '1901-00-00', 'Kainz', 'Kainz', 'Kainz', 'Kainz', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1911-00-00', '1914-00-00', 'KAN', 'KAN', 'KAN', 'KAN', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1905-00-00', '1907-00-00', 'Kronos', 'Kronos', 'Kronos', 'Kronos', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1891-00-00', '1901-00-00', 'Leesdorfer', 'Leesdorfer', 'Leesdorfer', 'Leesdorfer', null, null);


insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1952-00-00', '1955-00-00', 'Libelle', 'Libelle', 'Libelle', 'Libelle', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1921-00-00', '1928-00-00', 'Linett', 'Linett', 'Linett', 'Linett', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1906-00-00', '1908-00-00', 'Linser', 'Linser', 'Linser', 'Linser', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1996-00-00', '1906-00-00', 'Lohner-Porsche' , 'Lohner-Porsche' , 'Lohner-Porsche', 'Lohner-Porsche', null, 'https://ru.wikipedia.org/wiki/Lohner-Porsche');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1953-00-00', '1953-00-00', 'Möve', 'Möve', 'Möve', 'Möve', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1907-00-00', '1931-00-00', 'ÖAF', 'ÖAF', 'ÖAF', 'ÖAF', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1909-00-00', '1914-00-00', 'Perfekt', 'Perfekt', 'Perfekt', 'Perfekt', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1921-00-00', '1952-00-00', 'Perl', 'Perl', 'Perl', 'Perl', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1906-00-00', '1925-00-00', 'Puch', 'Puch', 'Puch', 'Puch', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1902-00-00', '1906-00-00', 'Spitz', 'Spitz', 'Spitz', 'Spitz', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1864-00-00', '2001-00-00', 'SDP', 'Steyr-Daimler-Puch', 'SDP', 'Steyr-Daimler-Puch', 'http://www.steyr-motors.com/', 'https://ru.wikipedia.org/wiki/Steyr-Daimler-Puch');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1920-00-00', '1977-00-00', 'Steyr', 'Steyr', 'Steyr', 'Steyr', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1907-00-00', '1908-00-00', 'T&G', 'Thein & Goldberger', 'T&G', 'Thein & Goldberger', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1900-00-00', '1900-00-00', 'Theyer Rothmund', 'Theyer Rothmund', 'Theyer Rothmund', 'Theyer Rothmund', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1977-00-00', '1995-00-00', 'Tomaszo', 'Tomaszo', 'Tomaszo', 'Tomaszo', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1919-00-00', '1923-00-00', 'U-Wagen', 'U-Wagen', 'U-Wagen', 'U-Wagen', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1911-00-00', '1925-00-00', 'WAF', 'WAF', 'WAF', 'WAF', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AT'), '1903-00-00', '1908-00-00', 'Wyner', 'Wyner', 'Wyner', 'Wyner', null, null);

# Ангола id = 27 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AO'), '2007-00-00', null, 'Zhongji', 'Zhongji', 'Zhongji', 'Zhongji', null, null);

# Аргентина id = 28 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1985-00-00', null, 'ASA', 'ASA', 'ASA', 'ASA', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1959-00-00', '1963-00-00', 'Adelmo', 'Adelmo', 'Adelmo', 'Adelmo', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1909-00-00', '1915-00-00', 'Anasagasti', 'Anasagasti', 'Anasagasti', 'Anasagasti', null, 'https://ru.wikipedia.org/wiki/Anasagasti');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1967-00-00', '1973-00-00', 'Andino', 'Andino', 'Andino', 'Andino', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1959-00-00', '1965-00-00', 'De Carlo', 'De Carlo', 'De Carlo', 'De Carlo', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1983-00-00', '1989-00-00', 'Eniak', 'Eniak', 'Eniak', 'Eniak', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1987-00-00', '1987-00-00', 'Feresa', 'Feresa', 'Feresa', 'Feresa', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1940-00-00', '1945-00-00', 'Hispano', 'Hispano-Argentina', 'Hispano', 'Hispano-Argentina', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1951-00-00', '1979-00-00', 'IAME', 'Industrias Aeronáuticas y Mecánicas del Estado(IAME)', 'IAME', 'Industrias Aeronáuticas y Mecánicas del Estado(IAME)', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1980-00-00', '1999-00-00', 'IES', 'IES', 'IES', 'IES', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1956-00-00', '1970-00-00', 'IKA', 'Industrias Kaiser Argentina (IKA)', 'IKA', 'Industrias Kaiser Argentina (IKA)', null, 'https://ru.wikipedia.org/wiki/Industrias_Kaiser_Argentina');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1960-00-00', '1960-00-00', 'Koller', 'Koller', 'Koller', 'Koller', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AR'), '1911-00-00', '1970-00-00', 'Siam di Tella', 'Siam di Tella', 'Siam di Tella', 'Siam di Tella', null, null);

# Армения id = 29 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='AM'), '1964-00-00', '2002-00-00', 'ErAZ', 'ErAZ', 'ЕрАЗ', 'ЕрАЗ', null, 'https://en.wikipedia.org/wiki/ErAZ');

# Бангладеш id = 30 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BD'), '1966-00-00', null, 'Pragoti', 'Pragoti', 'Pragoti', 'Pragoti', 'http://pragotiindustries.gov.bd/', 'https://en.wikipedia.org/wiki/Pragoti');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BD'), '1979-00-00', null, 'Rang\'s motor', 'Rang\'s motor', 'Rang\'s motor', 'Rang\'s motor', 'http://www.rangsmotors.com', null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BD'), '1977-00-00', null, 'Walton Hi-tech', 'Walton Hi-tech', 'Walton Hi-tech', 'Walton Hi-tech', 'http://www.waltonbd.com/', 'https://en.wikipedia.org/wiki/Walton_Hi-Tech_Industries_Limited');

# Беларусь id = 15 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1927-00-00', null, 'Amkodor', 'Amkodor', 'Амкодор', 'Амкодор', 'http://amkodor.by/', 'https://ru.wikipedia.org/wiki/%D0%90%D0%BC%D0%BA%D0%BE%D0%B4%D0%BE%D1%80');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1973-00-00', null, 'Belkommunmash', 'Belkommunmash', 'БелкоммунМаш', 'БелкоммунМаш', 'https://bkm.by', 'https://ru.wikipedia.org/wiki/%D0%91%D0%B5%D0%BB%D0%BA%D0%BE%D0%BC%D0%BC%D1%83%D0%BD%D0%BC%D0%B0%D1%88');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1948-00-00', null, 'BELAZ', 'BELAZ', 'БелАЗ', 'БелАЗ', 'http://www.belaz.by', 'https://en.wikipedia.org/wiki/BelAZ');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '2011-00-00', null, 'BELGEE', 'BELGEE', 'БелДжи', 'БелДжи', 'http://belgee.by/', 'https://ru.wikipedia.org/wiki/%D0%91%D0%B5%D0%BB%D0%94%D0%B6%D0%B8');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1998-00-00', null, 'ДорЭлектроМаш', 'ДорЭлектроМаш', 'ДорЭлектроМаш', 'ДорЭлектроМаш', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1944-00-00', null, 'MAZ', 'MAZ', 'МАЗ', 'МАЗ', 'http://maz.by/', 'https://en.wikipedia.org/wiki/Minsk_Automobile_Plant');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1954-00-00', null, 'MZKT', 'MZKT', 'МЗКТ', 'МЗКТ', 'http://www.mzkt.by/', 'https://en.wikipedia.org/wiki/MZKT');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BY'), '1935-00-00', null, 'MoAZ', 'MoAZ', 'МоАЗ', 'МоАЗ', 'http://www.moaz.ru/', 'https://en.wikipedia.org/wiki/MoAZ');

# Бельгия id = 31 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1984-00-00', null, 'Edran', 'Edran', 'Edran', 'Edran', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1994-00-00', null, 'Gillet', 'Gillet', 'Gillet', 'Gillet', 'http://www.gilletvertigo.com/', 'https://ru.wikipedia.org/wiki/Gillet');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1923-00-00', '1930-00-00', 'ADK', 'ADK', 'ADK', 'ADK', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1933-00-00', '1914-00-00', 'Alatac', 'Alatac', 'Alatac', 'Alatac', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1914-00-00', '1921-00-00', 'Alfa Legia', 'Alfa Legia', 'Alfa Legia', 'Alfa Legia', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1938-00-00', '1946-00-00', 'Altona', 'Altona', 'Altona', 'Altona', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1920-00-00', '1920-00-00', 'ALP', 'ALP', 'ALP', 'ALP', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1913-00-00', '1914-00-00', 'AMA', 'AMA', 'AMA', 'AMA', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1900-00-00', '1905-00-00', 'Antoine', 'Antoine', 'Antoine', 'Antoine', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1964-00-00', '1998-00-00', 'APAL', 'APAL', 'APAL', 'APAL', null, 'https://ru.wikipedia.org/wiki/Apal');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1900-00-00', '1903-00-00', 'Aquila', 'Aquila', 'Aquila', 'Aquila', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1914-00-00', '1914-00-00', 'ATA', 'ATA', 'ATA', 'ATA', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1930-00-00', '1930-00-00', 'Astra', 'Astra', 'Astra', 'Astra', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1905-00-00', '1912-00-00', 'Auto-Mixte', 'Auto-Mixte', 'Auto-Mixte', 'Auto-Mixte', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1907-00-00', '1909-00-00', 'Bastin', 'Bastin', 'Bastin', 'Bastin', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1904-00-00', '1906-00-00', 'Baudouin', 'Baudouin', 'Baudouin', 'Baudouin', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1920-00-00', '1921-00-00', 'Belga', 'Belga', 'Belga', 'Belga', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1928-00-00', '1937-00-00', 'Belga Rise', 'Belga Rise', 'Belga Rise', 'Belga Rise', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1909-00-00', 'Belgica', 'Belgica', 'Belgica', 'Belgica', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1908-00-00', '1914-00-00', 'Bovy', 'Bovy', 'Bovy', 'Bovy', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1914-00-00', '1914-00-00', 'CAP', 'CAP', 'CAP', 'CAP', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1953-00-00', '1980-00-00', 'Claeys-Flandria', 'Claeys-Flandria', 'Claeys-Flandria', 'Claeys-Flandria', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1912-00-00', '1927-00-00', 'd\'Aoust', 'd\'Aoust', 'd\'Aoust', 'd\'Aoust', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1894-00-00', '1924-00-00', 'Dasse', 'Dasse', 'Dasse', 'Dasse', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1903-00-00', '1906-00-00', 'De Cosmo', 'De Cosmo', 'De Cosmo', 'De Cosmo', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1899-00-00', 'Delecroix', 'Delecroix', 'Delecroix', 'Delecroix', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1901-00-00', 'Delin', 'Delin', 'Delin', 'Delin', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1906-00-00', 'Dechamps', 'Dechamps', 'Dechamps', 'Dechamps', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1923-00-00', '1923-00-00', 'De Wandre', 'De Wandre', 'De Wandre', 'De Wandre', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1904-00-00', '1905-00-00', 'Direct', 'Direct', 'Direct', 'Direct', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1912-00-00', '1914-00-00', 'Elgé', 'Elgé', 'Elgé', 'Elgé', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1925-00-00', '1926-00-00', 'Emmel', 'Emmel', 'Emmel', 'Emmel', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1926-00-00', '1929-00-00', 'Escol', 'Escol', 'Escol', 'Escol', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1901-00-00', '1932-00-00', 'Excelsior', 'Excelsior', 'Excelsior', 'Excelsior', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1912-00-00', '1914-00-00', 'FAB', 'FAB', 'FAB', 'FAB', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1923-00-00', '1929-00-00', 'FD', 'FD', 'FD', 'FD', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1909-00-00', '1914-00-00', 'FIF', 'FIF', 'FIF', 'FIF', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1920-00-00', '1921-00-00', 'Flaid', 'Flaid', 'Flaid', 'Flaid', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1939-00-00', 'FN', 'FN', 'FN', 'FN', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1906-00-00', '1912-00-00', 'Fondu', 'Fondu', 'Fondu', 'Fondu', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1914-00-00', '1914-00-00', 'Frenay', 'Frenay', 'Frenay', 'Frenay', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1897-00-00', '1917-00-00', 'Germain', 'Germain', 'Germain', 'Germain', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1906-00-00', '1948-00-00', 'Impéria', 'Impéria', 'Impéria', 'Impéria', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1925-00-00', '1926-00-00', 'Jeecy-Vea', 'Jeecy-Vea', 'Jeecy-Vea', 'Jeecy-Vea', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1895-00-00', '1905-00-00', 'Jenatzy', 'Jenatzy', 'Jenatzy', 'Jenatzy', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1923-00-00', '1928-00-00', 'Juwel', 'Juwel', 'Juwel', 'Juwel', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1952-00-00', '1952-00-00', 'Kleinstwagen', 'Kleinstwagen', 'Kleinstwagen', 'Kleinstwagen', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1898-00-00', '1909-00-00', 'Knap', 'Knap', 'Knap', 'Knap', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1900-00-00', '1914-00-00', 'Linon', 'Linon', 'Linon', 'Linon', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1902-00-00', '1906-00-00', 'Mathieu', 'Mathieu', 'Mathieu', 'Mathieu', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1980-00-00', '1984-00-00', 'Mathomobile', 'Mathomobile', 'Mathomobile', 'Mathomobile', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1964-00-00', '1974-00-00', 'Méan', 'Méan', 'Méan', 'Méan', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1955-00-00', '1972-00-00', 'Meeussen', 'Meeussen', 'Meeussen', 'Meeussen', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1898-00-00', '1928-00-00', 'Métallurgique', 'Métallurgique', 'Métallurgique', 'Métallurgique', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1896-00-00', '1926-00-00', 'Miesse', 'Miesse/Auto-Miesse', 'Miesse', 'Miesse/Auto-Miesse', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1902-00-00', '1939-00-00', 'Minerva', 'Minerva', 'Minerva', 'Minerva', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1925-00-00', '1927-00-00', 'Moustique', 'Moustique', 'Moustique', 'Moustique', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1900-00-00', '1928-00-00', 'Nagant', 'Nagant', 'Nagant', 'Nagant', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1914-00-00', '1914-00-00', 'Nova', 'Nova', 'Nova', 'Nova', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1912-00-00', '1914-00-00', 'Pescarolo', 'Pescarolo', 'Pescarolo', 'Pescarolo', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1899-00-00', 'Peterill', 'Peterill', 'Peterill', 'Peterill', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1912-00-00', 'Pieper', 'Pieper', 'Pieper', 'Pieper', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1898-00-00', '1914-00-00', 'Pipe', 'Pipe', 'Pipe', 'Pipe', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1954-00-00', '1955-00-00', 'PLM', 'PLM', 'PLM', 'PLM', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1922-00-00', '1924-00-00', 'P-M', 'P-M', 'P-M', 'P-M', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1957-00-00', '1960-00-00', 'Radar', 'Radar', 'Radar', 'Radar', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1970-00-00', '1976-00-00', 'Ranger', 'Ranger', 'Ranger', 'Ranger', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1904-00-00', '1910-00-00', 'Royal Star', 'Royal Star', 'Royal Star', 'Royal Star', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1899-00-00', 'Rumpf', 'Rumpf', 'Rumpf', 'Rumpf', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1910-00-00', '1923-00-00', 'SAVA', 'SAVA', 'SAVA', 'SAVA', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1927-00-00', '1928-00-00', 'SCH', 'SCH', 'SCH', 'SCH', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1921-00-00', '1922-00-00', 'SOMEA', 'SOMEA', 'SOMEA', 'SOMEA', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1924-00-00', '1927-00-00', 'Speedsport', 'Speedsport', 'Speedsport', 'Speedsport', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1907-00-00', '1914-00-00', 'Springuel', 'Springuel', 'Springuel', 'Springuel', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1914-00-00', '1922-00-00', 'Taunton', 'Taunton', 'Taunton', 'Taunton', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1920-00-00', '1925-00-00', 'TVD', 'TVD', 'TVD', 'TVD', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1895-00-00', '1905-00-00', 'Vincke', 'Vincke', 'Vincke', 'Vincke', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1899-00-00', '1914-00-00', 'Vivinus', 'Vivinus', 'Vivinus', 'Vivinus', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1897-00-00', '1901-00-00', 'Wilford', 'Wilford', 'Wilford', 'Wilford', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BE'), '1958-00-00', '1962-00-00', 'Zelensis', 'Zelensis', 'Zelensis', 'Zelensis', null, null);

# Ботсвана id = 32 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BW'), '2006-00-00', null, 'HSC', 'Harper Sports Cars', 'HSC', 'Harper Sports Cars', null, null);

# Бразилия id = 33 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BR'), '1962-00-00', null, 'Agrale', 'Agrale', 'Agrale', 'Agrale', 'http://www.agrale.com.br', 'https://ru.wikipedia.org/wiki/Agrale');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BR'), '1999-00-00', null, 'Lobini', 'Lobini', 'Lobini', 'Lobini', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BR'), '2004-00-00', null, 'TAC Motors', 'TAC Motors', 'TAC Motors', 'TAC Motors', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BR'), '1995-00-00', null, 'Troller', 'Troller', 'Troller', 'Troller', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BR'), '1974-00-00', '1990-00-00', 'MP-Lafer', 'MP-Lafer', 'MP-Lafer', 'MP-Lafer', null, null);


# Болгария id = 34 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BG'), '1966-00-00', '1970-00-00', 'Bulgarrenault', 'Bulgarrenault', 'Bulgarrenault', 'Bulgarrenault', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BG'), '1966-00-00', '1970-00-00', 'Bulgaralpine', 'Bulgaralpine', 'Bulgaralpine', 'Bulgaralpine', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BG'), '1966-00-00', '1990-00-00', 'Moskvitch Aleko', 'Moskvitch Aleko', 'Moskvitch Aleko', 'Moskvitch Aleko', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BG'), '1967-00-00', '1971-00-00', 'Pirin-Fiat', 'Pirin-Fiat', 'Pirin-Fiat', 'Pirin-Fiat', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='BG'), '1980-00-00', '1990-00-00', 'Sofia', 'Sofia', 'Sofia', 'Sofia', null, null);

# Великобритания id = 3 ===================================


insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1902-00-00', null, 'AC', 'AC', 'AC', 'AC', 'http://www.accars.eu/', 'https://ru.wikipedia.org/wiki/AC_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1936-00-00', '1966-00-00', 'Allard', 'Allard', 'Allard', 'Allard', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1919-00-00', '1967-00-00', 'Alvis', 'Alvis', 'Alvis', 'Alvis', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1919-00-00', '1960-00-00', 'Armstrong', 'Armstrong Siddeley', 'Armstrong', 'Armstrong Siddeley', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1995-00-00', null, 'Ascari', 'Ascari', 'Ascari', 'Ascari', 'https://www.ascari.net/', 'https://ru.wikipedia.org/wiki/Ascari');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1913-00-00', null, 'Aston Martin', 'Aston Martin', 'Aston Martin', 'Aston Martin', 'http://www.astonmartin.com/', 'https://ru.wikipedia.org/wiki/Aston_Martin');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1953-00-00', '1971-00-00', 'Austin-Healey', 'Austin-Healey', 'Austin-Healey', 'Austin-Healey', 'http://www.austinhealey.com/', 'https://ru.wikipedia.org/wiki/Austin-Healey');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1919-00-00', null, 'Bentley Motors', 'Bentley Motors', 'Bentley Motors', 'Bentley Motors', 'http://www.bentleymotors.com/', 'https://ru.wikipedia.org/wiki/Bentley_Motors');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1945-00-00', null, 'Bristol', 'Bristol', 'Bristol', 'Bristol', 'http://www.bristolcars.co.uk/', 'https://ru.wikipedia.org/wiki/Bristol_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1968-00-00', null, 'British Leyland', 'British Leyland', 'British Leyland', 'British Leyland', null, 'https://ru.wikipedia.org/wiki/British_Leyland_Motor_Corporation');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1963-00-00', null, 'Caterham', 'Caterham', 'Caterham', 'Caterham', 'http://uk.caterhamcars.com/', 'https://ru.wikipedia.org/wiki/Caterham_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1883-00-00', null, 'Daimler', 'Daimler', 'Daimler', 'Daimler', null, 'https://ru.wikipedia.org/wiki/Daimler_Motor_Company');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1958-00-00', null, 'Ginetta', 'Ginetta', 'Ginetta', 'Ginetta', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1964-00-00', '1967-00-00', 'Gordon-Keeble', 'Gordon-Keeble', 'Gordon-Keeble', 'Gordon-Keeble', null, 'https://en.wikipedia.org/wiki/Gordon-Keeble');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1907-00-00', '1931-00-00', 'Hillman', 'Hillman', 'Hillman', 'Hillman', null, 'https://en.wikipedia.org/wiki/Hillman');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1887-00-00', '1967-00-00', 'Humber', 'Humber', 'Humber', 'Humber', null, 'https://en.wikipedia.org/wiki/Humber_Limited');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1922-00-00', null, 'Jaguar', 'Jaguar', 'Jaguar', 'Jaguar', 'http://www.jaguar.com', 'https://ru.wikipedia.org/wiki/Jaguar');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1934-00-00', '1976-00-00', 'Jensen', 'Jensen', 'Jensen', 'Jensen', null, 'https://ru.wikipedia.org/wiki/Jensen_Motors');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1906-00-00', '1955-00-00', 'Jowett', 'Jowett', 'Jowett', 'Jowett', null, 'https://en.wikipedia.org/wiki/Jowett_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1895-00-00', '1955-00-00', 'Lanchester', 'Lanchester', 'Lanchester', 'Lanchester', null, 'https://en.wikipedia.org/wiki/Lanchester_Motor_Company');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1948-00-00', null, 'Land Rover', 'Land Rover', 'Land Rover', 'Land Rover', 'http://www.landrover.com', 'https://en.wikipedia.org/wiki/Land_Rover');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1952-00-00', null, 'Lotus', 'Lotus', 'Lotus', 'Lotus', 'http://www.lotuscars.com/', 'https://en.wikipedia.org/wiki/Lotus_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1959-00-00', '2007-00-00', 'Marcos', 'Marcos', 'Marcos', 'Marcos', 'http://marcos-eng.com/', 'https://en.wikipedia.org/wiki/Marcos_Engineering');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1924-00-00', null, 'MG Cars', 'MG Cars', 'MG Cars', 'MG Cars', 'http://mg.co.uk/', 'https://en.wikipedia.org/wiki/MG_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1958-00-00', '2000-00-00', 'Mini Cooper', 'Mini Cooper', 'Mini Cooper', 'Mini Cooper', null, 'https://en.wikipedia.org/wiki/Mini');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1909-00-00', null, 'Morgan', 'Morgan', 'Morgan', 'Morgan', 'https://www.morgan-motor.co.uk/', 'https://en.wikipedia.org/wiki/Morgan_Motor_Company');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1912-00-00', '1952-00-00', 'Morris', 'Morris', 'Morris', 'Morris', null, 'https://en.wikipedia.org/wiki/Morris_Motors');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1999-00-00', null, 'Noble', 'Noble', 'Noble', 'Noble', 'http://www.noblecars.com/', 'https://en.wikipedia.org/wiki/Noble_Automotive');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1904-00-00', null, 'Rolls Royce', 'Rolls Royce', 'Rolls Royce', 'Rolls Royce', 'https://www.rolls-roycemotorcars.com', 'https://en.wikipedia.org/wiki/Rolls-Royce_Motor_Cars');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1887-00-00', '2005-00-00', 'Rover', 'Rover', 'Rover', 'Rover', null, 'https://en.wikipedia.org/wiki/Rover_Company');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1888-00-00', '1976-00-00', 'Sunbeam', 'Sunbeam', 'Sunbeam', 'Sunbeam', null, 'https://en.wikipedia.org/wiki/Sunbeam_Motor_Car_Company');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1903-00-00', '1992-00-00', 'Talbot', 'Talbot', 'Talbot', 'Talbot', null, 'https://en.wikipedia.org/wiki/Talbot');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1885-00-00', '1984-00-00', 'Triumph', 'Triumph', 'Triumph', 'Triumph', null, 'https://en.wikipedia.org/wiki/Triumph_Motor_Company');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1946-00-00', '2012-00-00', 'TVR', 'TVR', 'TVR', 'TVR', null, 'https://en.wikipedia.org/wiki/TVR');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1945-00-00', null, 'Vauxhall', 'Vauxhall', 'Vauxhall', 'Vauxhall', 'http://www.vauxhall.co.uk/', 'https://en.wikipedia.org/wiki/Vauxhall_Motors');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GB'), '1901-00-00', '1975-00-00', 'Wolseley', 'Wolseley', 'Wolseley', 'Wolseley', null, 'https://en.wikipedia.org/wiki/Wolseley_Motors');

# Венгрия id = 19 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1986-00-00', '1990-00-00', 'Borbála', 'Borbála', 'Borbála', 'Borbála', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1949-00-00', '1996-00-00', 'Csepel', 'Csepel', 'Csepel', 'Csepel', null, 'https://en.wikipedia.org/wiki/Csepel_(automobile)');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1909-00-00', '1924-00-00', 'Csonka', 'Csonka', 'Csonka', 'Csonka', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1923-00-00', '1932-00-00', 'Fejes', 'Fejes', 'Fejes', 'Fejes', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1928-00-00', '1929-00-00', 'Ha', 'Ha', 'Ha', 'Ha', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1895-00-00', '2007-00-00', 'Ikarus', 'Ikarus', 'Ikarus', 'Ikarus', null, 'https://en.wikipedia.org/wiki/Ikarus_Bus');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1911-00-00', '1934-00-00', 'MÁG', 'MÁG', 'MÁG', 'MÁG', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1901-00-00', '1931-00-00', 'Magomobil', 'Magomobil', 'Magomobil', 'Magomobil', null, 'https://en.wikipedia.org/wiki/Hungarian_General_Machine_Factory');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1908-00-00', '1922-00-00', 'Marta', 'Marta', 'Marta', 'Marta', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1938-00-00', '1942-00-00', 'Mávag', 'Mávag', 'Mávag', 'Mávag', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1923-00-00', '1934-00-00', 'Méray', 'Méray', 'Méray', 'Méray', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1904-00-00', '1912-00-00', 'Phönix', 'Phönix', 'Phönix', 'Phönix', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1990-00-00', '1997-00-00', 'Puli', 'Puli', 'Puli', 'Puli', null, null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1896-00-00', null, 'RÁBA', 'RÁBA', 'Rába', 'Rába', 'http://www.raba.hu', 'https://en.wikipedia.org/wiki/R%C3%A1ba_(company)');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='HU'), '1927-00-00', '1932-00-00', 'Weiss Manfréd', 'Weiss Manfréd', 'Weiss Manfréd', 'Weiss Manfréd', null, null);

# Вьетнам id = 35 ===================================


insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='VN'), '1991-00-00', null, 'Mekong Auto', 'Mekong Auto', 'Mekong Auto', 'Mekong Auto', 'http://www.mekongauto.com.vn/', 'https://en.wikipedia.org/wiki/Mekong_Auto');

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='VN'), '1997-00-00', null, 'Thaco', 'Thaco', 'Thaco', 'Thaco', 'http://www.thacogroup.vn', null);

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='VN'), '2004-00-00', null, 'Vinaxuki', 'Vinaxuki', 'Vinaxuki', 'Vinaxuki', null, 'https://en.wikipedia.org/wiki/Vinaxuki');

# Гана id = 36 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='GH'), '2010-00-00', null, 'Solaris', 'Solaris', 'Solaris', 'Solaris', null, null);

# Германия id = 4 ===================================

insert into `manufacturers`
	(country_id, foundation_date, termination_date, name_short_en, name_full_en, name_short_orig, name_full_orig,  website, wikipage)
values
	((select country_id from countries where iso_code_2='DE'), '1939-00-00', null, 'RUF', 'RUF', 'RUF', 'RUF', 'http://ruf-automobile.de', 'https://en.wikipedia.org/wiki/Ruf_Automobile');



