/*
 * FloatProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class FloatProperty
	extends AbstractProperty <Float>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Float DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public FloatProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public FloatProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public FloatProperty(final String name, final Float defaultValue)
	{
		super (name, defaultValue);
	}

	public FloatProperty(final Long id, final String name, final Float defaultValue)
	{
		super (id, name, defaultValue);
	}

	public FloatProperty(final String name, final Float defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public FloatProperty(final Long id, final String name, final Float defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : Float.parseFloat(value));
	}
}
