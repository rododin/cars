/*
 * STWDetector.java
 */

package org.rododin.cars.generic.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Description.
 * @author Rod Odin
 */
public class STWDetector
{
// Interface Types -----------------------------------------------------------------------------------------------------

	public interface LogConfig
	{
		Level getLogLevel();
		long getInterval();
	}

	public static class StandardLogConfig
		extends LogConfigImpl
	{
		public StandardLogConfig(Level logLevel, long interval)
		{
			super(logLevel, interval);
		}
	}

// Constants -----------------------------------------------------------------------------------------------------------

	public static final Logger DEFAULT_DETECTOR_LOG       = LogManager.getLogger(STWDetector.class.getName());
	public static final Logger  DEFAULT_STATS_LOG         = DEFAULT_DETECTOR_LOG;
	public static final boolean DEFAULT_DETECTOR_ENABLED  = true;
	public static final boolean DEFAULT_STATS_ENABLED     = true;
	public static final long    DEFAULT_DETECTOR_INTERVAL = 10; // ms
	public static final long    DEFAULT_STATS_INTERVAL    = 60 * 60 * 1000; // ms

	public static final LogConfig DEFAULT_DEBUG_LOG_CONFIG = new LogConfigImpl(Level.DEBUG,   50L); // ms
	public static final LogConfig DEFAULT_INFO_LOG_CONFIG  = new LogConfigImpl(Level.INFO ,  250L); // ms
	public static final LogConfig DEFAULT_WARN_LOG_CONFIG  = new LogConfigImpl(Level.WARN , 1000L); // ms
	public static final LogConfig DEFAULT_FATAL_LOG_CONFIG = new LogConfigImpl(Level.FATAL, 5000L); // ms

// Constructors --------------------------------------------------------------------------------------------------------

	public STWDetector()
	{
		this( DEFAULT_DETECTOR_LOG     , DEFAULT_STATS_LOG
		    , DEFAULT_DETECTOR_ENABLED , DEFAULT_STATS_ENABLED
		    , DEFAULT_DETECTOR_INTERVAL, DEFAULT_STATS_INTERVAL
		    , DEFAULT_DEBUG_LOG_CONFIG
		    , DEFAULT_INFO_LOG_CONFIG
		    , DEFAULT_WARN_LOG_CONFIG
		    , DEFAULT_FATAL_LOG_CONFIG
		);
	}

	public STWDetector( Logger       detectorLog
	                  , Logger       statsLog
	                  , boolean      detectorEnabled
	                  , boolean      statsEnabled
	                  , long         detectorInterval
	                  , long         statsInterval
	                  , LogConfig... logConfigs
	                  )
	{
		Assert.assertNotNull (detectorLog          );
		Assert.assertNotNull (statsLog             );
		Assert.assertTrue    (detectorInterval  > 0);
		Assert.assertTrue    (statsInterval     > 0);
		Assert.assertTrue    (logConfigs.length > 0);

		this.detectorLog = detectorLog;
		this.detectorEnabled.set(detectorEnabled);
		this.statsLog = statsLog;
		this.statsEnabled.set(statsEnabled);
		this.detectorInterval = detectorInterval;
		this.statsInterval = statsInterval;

		final List<LogConfig> logConfigsList = new ArrayList<LogConfig>(Arrays.asList(logConfigs));
		logConfigsList.sort(new LogConfigComparator());
		this.logConfigs = Collections.unmodifiableSet(new LinkedHashSet<LogConfig>(logConfigsList));

		for (LogConfig logConfig : this.logConfigs)
		{
			periodicStats.put(logConfig, new AtomicInteger());
			summaryStats.put(logConfig, new AtomicInteger());
		}

		start();
	}

// Service Methods -----------------------------------------------------------------------------------------------------

	public void start()
	{
		if (this.detector.get() == null)
		{
			final Thread detector = new Thread(new DetectionTask(), STWDetector.class.getSimpleName() + INSTANCE_COUNT.incrementAndGet());
			if (this.detector.compareAndSet(null, detector))
			{
				detector.setPriority(Thread.MAX_PRIORITY);
				detector.setDaemon(true);
				detector.start();

				detectorLog.info( getClass().getSimpleName()
				                + " started: detectorLog=" + detectorLog.getName()
				                + ", statsLog="            + statsLog.getName()
				                + ", detectorEnabled="     + detectorEnabled
				                + ", statsEnabled="        + statsEnabled
				                + ", detectorInterval="    + detectorInterval
				                + ", statsInterval="       + statsInterval
				                + ", logConfigs="          + this.logConfigs
				                );
			}
		}
	}

	public void terminate()
	{
		final Thread detector = this.detector.get();
		if (detector != null)
		{
			if (this.detector.compareAndSet(detector, null))
			{
				detector.interrupt();

				detectorLog.info( getClass().getSimpleName()
				                + " terminated: detectorLog=" + detectorLog.getName()
				                + ", statsLog="               + statsLog.getName()
				                + ", detectorEnabled="        + detectorEnabled
				                + ", statsEnabled="           + statsEnabled
				                + ", detectorInterval="       + detectorInterval
				                + ", statsInterval="          + statsInterval
				                + ", logConfigs="             + this.logConfigs
				                );
			}
		}
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public Logger getDetectorLog()
	{
		return detectorLog;
	}

	public boolean isDetectorEnabled()
	{
		return detectorEnabled.get();
	}

	public void setDetectorEnabled(final boolean detectorEnabled)
	{
		this.detectorEnabled.set(detectorEnabled);
	}

	public Logger getStatsLog()
	{
		return statsLog;
	}

	public boolean isStatsEnabled()
	{
		return statsEnabled.get();
	}

	public void setStatsEnabled(final boolean statsEnabled)
	{
		this.statsEnabled.set(statsEnabled);
	}

	public long getDetectorInterval()
	{
		return detectorInterval;
	}

	public long getStatsInterval()
	{
		return statsInterval;
	}

	public Set<LogConfig> getLogConfigs()
	{
		return logConfigs;
	}

	public Map<LogConfig, Integer> getPeriodicStats()
	{
		return doGetStats(periodicStats);
	}

	public Map<LogConfig, Integer> getSummaryStats()
	{
		return doGetStats(summaryStats);
	}

// Implementation ------------------------------------------------------------------------------------------------------

	private static class LogConfigImpl
		implements LogConfig
	{
		public LogConfigImpl(Level logLevel, long interval)
		{
			Assert.assertNotNull(logLevel);
			Assert.assertTrue(interval > 0);

			this.logLevel = logLevel;
			this.interval = interval;
		}

		public Level getLogLevel()
		{
			return logLevel;
		}

		public long getInterval()
		{
			return interval;
		}

		@Override
		public boolean equals(final Object o)
		{
			if (this == o)
				return true;
			if (o == null || getClass() != o.getClass())
				return false;

			final LogConfigImpl logConfig = (LogConfigImpl) o;
			return interval == logConfig.interval && logLevel.equals(logConfig.logLevel);
		}

		@Override
		public int hashCode()
		{
			int result = logLevel.hashCode();
			result = 31 * result + (int) (interval ^ (interval >>> 32));
			return result;
		}

		@Override
		public String toString()
		{
			return getClass().getSimpleName()
			     + "{logLevel=" + logLevel
			     + ", interval=" + interval
			     + '}';
		}

		private final Level logLevel;
		private final long  interval;
	}

	private static class LogConfigComparator implements Comparator<LogConfig>
	{
		public int compare(final LogConfig o1, final LogConfig o2)
		{
			return -Long.compare(o1.getInterval(), o2.getInterval());
		}
	}

	private class DetectionTask implements Runnable
	{
		public void run()
		{
			int errorCount = 0;
			while (true)
			{
				try
				{
					if (detectorEnabled.get())
					{
						final long currentTime = System.currentTimeMillis();
						final long timeDiff = currentTime - previousTime;
						final long statsTimeDiff = currentTime - lastStatsTime;

						if (timeDiff < 0)
							detectorLog.warn(STWDetector.class.getSimpleName() + ": Unexpected system time update detected: timeDiff=" + timeDiff);
						else
						{
							for (LogConfig logConfig : logConfigs)
							{
								if (timeDiff >= logConfig.getInterval())
								{
									periodicStats.get(logConfig).incrementAndGet();
									summaryStats.get(logConfig).incrementAndGet();
									detectorLog.log(logConfig.getLogLevel(), STWDetector.class.getSimpleName() + ": STW detected: timeDiff=" + timeDiff);
									break;
								}
							}
						}

						previousTime = currentTime;

						if (statsEnabled.get() && statsTimeDiff >= statsInterval)
						{
							final StringBuilder statsInfo = new StringBuilder(STWDetector.class.getSimpleName()).append(": STW Stats:"); // period=").append(statsInterval);
							for (LogConfig logConfig : logConfigs)
								statsInfo.append("\n  >= ").append(logConfig.getInterval()).append("ms -> duringLastPeriod=").append(periodicStats.get(logConfig).getAndSet(0)).append(", sinceStart=").append(summaryStats.get(logConfig).get());
							statsLog.info(statsInfo);

							lastStatsTime = currentTime;
						}
					}
					errorCount = 0;
				}
				catch (Exception x)
				{
					detectorLog.error(STWDetector.class.getSimpleName() + ": Detection error", x);
					if (++errorCount >= 3)
					{
						detectorLog.error(STWDetector.class.getSimpleName() + " failed", x);
						break;
					}
				}

				try
				{
					Thread.sleep(detectorInterval);
				}
				catch (InterruptedException x)
				{
					break;
				}
			}

			detectorLog.warn(STWDetector.class.getSimpleName() + " terminated");
		}

		private long previousTime = System.currentTimeMillis();
		private long lastStatsTime = System.currentTimeMillis();
	}

	private static Map<LogConfig, Integer> doGetStats(ConcurrentMap<LogConfig, AtomicInteger> stats)
	{
		final Map<LogConfig, Integer> rv = new TreeMap<>(new LogConfigComparator());
		for (Map.Entry<LogConfig, AtomicInteger> statsEntry : stats.entrySet())
			rv.put(statsEntry.getKey(), statsEntry.getValue().get());
		return Collections.unmodifiableMap(rv);
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final static AtomicInteger INSTANCE_COUNT = new AtomicInteger();

	private final Logger detectorLog;
	private final Logger statsLog;

	private final AtomicBoolean detectorEnabled = new AtomicBoolean();
	private final AtomicBoolean statsEnabled = new AtomicBoolean();

	private final long detectorInterval;
	private final long statsInterval;

	private final Set<LogConfig> logConfigs;

	private final ConcurrentMap<LogConfig, AtomicInteger> periodicStats = new ConcurrentHashMap<>();
	private final ConcurrentMap<LogConfig, AtomicInteger> summaryStats = new ConcurrentHashMap<>();

	private final AtomicReference<Thread> detector = new AtomicReference<>();
}
