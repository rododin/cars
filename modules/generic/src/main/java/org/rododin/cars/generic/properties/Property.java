/*
 * Property.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public interface Property <Value>
{
	Long getId();
	String getName();

	Value getDefaultValue();
	String getDefaultValueAsString();

	Value getValue();
	void setValue(Value value);

	String getValueAsString();
	void setValueAsString(String value);

	boolean isEnabled();
	void setEnabled(boolean enabled);

	boolean isWritable();
	void setWritable(boolean writable);

	boolean isImportant();
	void setImportant(boolean important);
}
