/*
 * ByteProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class ByteProperty
	extends AbstractProperty <Byte>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Byte DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public ByteProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public ByteProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public ByteProperty(final String name, final Byte defaultValue)
	{
		super (name, defaultValue);
	}

	public ByteProperty(final Long id, final String name, final Byte defaultValue)
	{
		super (id, name, defaultValue);
	}

	public ByteProperty(final String name, final Byte defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public ByteProperty(final Long id, final String name, final Byte defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : Byte.parseByte(value));
	}
}
