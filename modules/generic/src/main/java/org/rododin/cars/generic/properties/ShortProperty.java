/*
 * ShortProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class ShortProperty
	extends AbstractProperty <Short>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Short DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public ShortProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public ShortProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public ShortProperty(final String name, final Short defaultValue)
	{
		super (name, defaultValue);
	}

	public ShortProperty(final Long id, final String name, final Short defaultValue)
	{
		super (id, name, defaultValue);
	}

	public ShortProperty(final String name, final Short defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public ShortProperty(final Long id, final String name, final Short defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : Short.parseShort(value));
	}
}
