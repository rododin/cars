/*
 * Log4JOutputStreamWrapper.java
 */

package org.rododin.cars.generic.log;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rododin.cars.generic.utils.Assert;

/**
 * Description.
 * @author Rod Odin
 */
public class Log4JOutputStreamWrapper
	extends OutputStream
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Logger STDERR_LOG = LogManager.getLogger(Log4JOutputStreamWrapper.class);

	private static final int DEFAULT_BUFFER_LENGTH = 4096;
	private static final String LINE_SEPARATOR = System.getProperty("line.separator");

// Attributes ----------------------------------------------------------------------------------------------------------

	private final Logger  logger;
	private final Level   level;

	private int     bufLength;
	private byte[]  buf;
	private boolean hasBeenClosed;
	private int     count;

// Constructors --------------------------------------------------------------------------------------------------------

	public Log4JOutputStreamWrapper(Logger logger, Level level)
		throws NullPointerException
	{
		Assert.assertNotNull(logger, "Logger must not be null");
		Assert.assertNotNull(logger, "Level must not be null" );

		this.logger    = logger;
		this.level     = level;
		this.bufLength = DEFAULT_BUFFER_LENGTH;
		this.buf       = new byte[DEFAULT_BUFFER_LENGTH];
		this.count     = 0;
	}

// Service Methods -----------------------------------------------------------------------------------------------------

	/**
	 * Redirects STDERR and/or STDOUT to Log4J system.
	 * Redirection of STDERR is done with <code>WARN</code> logging level,
	 * and redirection of STDOUT - with <code>INFO</code>.
	 * A good place to invoke this method is a static context of your main class (class with the main routine or
	 * main servlet class).
	 * @param redirectStdErr <code>true</code> to redirect STDERR. Please note, if you redirect STDERR to Log4J,
	 *                       you must not use <code>System.err</code> for <code>ConsoleAppender</code> in your Log4J
	 *                       configuration.
	 * @param redirectStdOut <code>true</code> to redirect STDOUT. Please note, if you redirect STDOUT to Log4J,
	 *                       you must not use <code>System.out</code> for <code>ConsoleAppender</code> in your Log4J
	 *                       configuration.
	 */
	public static void redirect(boolean redirectStdErr, boolean redirectStdOut)
	{
		if (redirectStdErr)
		{
			System.setErr(new PrintStream(new Log4JOutputStreamWrapper(STDERR_LOG, Level.WARN), true));
			STDERR_LOG.info("STDERR is redirected to Log4J");
		}

		if (redirectStdOut)
		{
			System.setOut(new PrintStream(new Log4JOutputStreamWrapper(STDERR_LOG, Level.INFO), true));
			STDERR_LOG.info("STDOUT is redirected to Log4J");
		}
	}

// Interface Methods ---------------------------------------------------------------------------------------------------

	public void write(final int b)
		throws IOException
	{
		if (hasBeenClosed)
			throw new IOException("The stream has been closed");

		if (count == bufLength)
		{
			final int newBufLength = bufLength + DEFAULT_BUFFER_LENGTH;
			final byte[] newBuf = new byte[newBufLength];
			System.arraycopy(buf, 0, newBuf, 0, bufLength);
			buf = newBuf;
			bufLength = newBufLength;
		}
		buf[count] = (byte) b;
		count++;
	}

	@Override
	public void flush()
	{
		if (count == 0)
			return;
		// don't print out blank lines; flushing from PrintStream puts out these
		if (LINE_SEPARATOR.equals(new String(buf, 0, LINE_SEPARATOR.length())))
		{
			reset();
			return;
		}
		final byte[] theBytes = new byte[count];
		System.arraycopy(buf, 0, theBytes, 0, count);
		logger.log(level, new String(theBytes));
		reset();
	}

	@Override
	public void close()
	{
		flush();
		hasBeenClosed = true;
	}

	private void reset()
	{
		count = 0;
	}
}
