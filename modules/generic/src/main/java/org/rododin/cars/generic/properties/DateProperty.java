/*
 * DateProperty.java
 */

package org.rododin.cars.generic.properties;

import java.text.ParseException;
import java.util.Date;

import org.rododin.cars.generic.utils.DateTimeUtils;

/**
 * Description.
 * @author Rod Odin
 */
public class DateProperty
	extends AbstractProperty <Date>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Date DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public DateProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public DateProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public DateProperty(final String name, final Date defaultValue)
	{
		super (name, defaultValue);
	}

	public DateProperty(final Long id, final String name, final Date defaultValue)
	{
		super (id, name, defaultValue);
	}

	public DateProperty(final String name, final Date defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public DateProperty(final Long id, final String name, final Date defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	@Override
	public String getDefaultValueAsString()
	{
		final Date value = getDefaultValue();
		return value == null ? NULL_STRING_VALUE : DateTimeUtils.fromDate(value);
	}

	@Override
	public String getValueAsString()
	{
		final Date value = getValue();
		return value == null ? NULL_STRING_VALUE : DateTimeUtils.fromDate(value);
	}

	public void setValueAsString(final String value)
	{
		try
		{
			setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : DateTimeUtils.toDate(value));
		}
		catch (ParseException x)
		{
			throw new RuntimeException(x);
		}
	}
}
