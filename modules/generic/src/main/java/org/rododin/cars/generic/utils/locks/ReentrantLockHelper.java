/*
 * ReentrantLockHelper.java
 */

package org.rododin.cars.generic.utils.locks;

import java.util.concurrent.Callable;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Introduces a helper mechanism to simplify the usage code of the <code>{@link ReentrantReadWriteLock}</code>.
 * The interface provides the default typical implementation of the <code>{@link #run(LockType, Runnable)}</code>
 * and <code>{@link #call(LockType, Callable)}</code> methods, meanwhile the descendants should implement
 * the <code>{@link #lock(LockType)}</code> and <code>{@link #unlock(LockType)}</code> ones.
 */
public interface ReentrantLockHelper
{
// Constants -----------------------------------------------------------------------------------------------------------

	/**
	 * Use <code>{@link #READ}</code> to acquire a lock for read-only purposes
	 * (when you need to only read the shared data to be maintained with the lock),
	 * and use <code>{@link #WRITE}</code> if you need to change some shared data in the critical section maintained
	 * by the lock.
	 */
	enum LockType { READ, WRITE }

// Lock/Unlock Methods -------------------------------------------------------------------------------------------------

	/**
	 * Acquires a lock with given type.
	 * @param lockType the lock type to acquire
	 * @see #unlock(LockType)
	 */
	void lock(LockType lockType);

	/**
	 * Releases the previously acquired lock of the given type.
	 * @param lockType the lock type to release
	 * @see #lock(LockType)
	 */
	void unlock(LockType lockType);

// Runnable/Callable ---------------------------------------------------------------------------------------------------

	/**
	 * Executes the given <code>{@link Runnable}</code> task while holding a lock.
	 * It doesn't return any values and hides any exceptions thrown from the <code>{@link Runnable}</code> code.
	 * @param lockType the lock type to acquire/release
	 * @param runnable the <code>{@link Runnable}</code> task to be executed
	 * @see #call(LockType, Callable)
	 */
	default void run(LockType lockType, Runnable runnable)
	{
		lock(lockType);
		try
		{
			runnable.run();
		}
		finally
		{
			unlock(lockType);
		}
	}

	/**
	 * Executes the given <code>{@link Callable}</code> task while holding a lock.
	 * It returns a value of the execution according to the <code>{@link Callable}</code> protocol,
	 * as well as it rethrows the exception if it gets thrown from the <code>{@link Callable}</code> code.
	 * @param lockType the lock type to acquire/release
	 * @param callable the <code>{@link Callable}</code> task to be executed
	 * @return the result of <code>{@link Callable}</code>
	 * @see #run(LockType, Runnable)
	 */
	default  <T> T call(LockType lockType, Callable<T> callable)
	{
		lock(lockType);
		try
		{
			return callable.call();
		}
		catch (Exception x)
		{
			throw new RuntimeException(x);
		}
		finally
		{
			unlock(lockType);
		}
	}
}
