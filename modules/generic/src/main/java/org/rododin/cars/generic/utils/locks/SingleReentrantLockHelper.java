/*
 * ReentrantLockHelperSingle.java
 */

package org.rododin.cars.generic.utils.locks;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * A simple<code>{@link ReentrantLockHelper}</code> implementation maintaining a single
 * <code>{@link ReentrantReadWriteLock}</code>.
 */
public class SingleReentrantLockHelper
	implements ReentrantLockHelper
{
// Constructors --------------------------------------------------------------------------------------------------------

	public SingleReentrantLockHelper(ReentrantReadWriteLock lock)
	{
		this.lock = lock;
	}

	public SingleReentrantLockHelper()
	{
		this(new ReentrantReadWriteLock());
	}

// Lock/Unlock Methods -------------------------------------------------------------------------------------------------

	public void lock(LockType lockType)
	{
		(lockType == LockType.WRITE ? lock.writeLock() : lock.readLock()).lock();
	}

	public void unlock(LockType lockType)
	{
		(lockType == LockType.WRITE ? lock.writeLock() : lock.readLock()).unlock();
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final ReentrantReadWriteLock lock;
}
