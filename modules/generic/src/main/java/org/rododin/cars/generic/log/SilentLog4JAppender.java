/*
 * SilentLog4JAppender.java
 */

package org.rododin.cars.generic.log;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.ErrorHandler;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.DefaultErrorHandler;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.layout.ByteBufferDestination;

/**
 * Suppresses any logging. It's useful for substitution to 3rd party libs to suppress their logging.
 * @author Rod Odin
 */
@Plugin(name = "SilentLog4JAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE, printObject = true)
public class SilentLog4JAppender
	implements Appender
{
// Attributes ----------------------------------------------------------------------------------------------------------

	private final String name;

// Constructors & Factory Methods --------------------------------------------------------------------------------------

	private SilentLog4JAppender(String name)
	{
		this.name = name;
	}

	@PluginFactory
	public static SilentLog4JAppender createAppender(@PluginAttribute("name") String name)
	{
		return new SilentLog4JAppender(name);
	}

// Appender Implementation ---------------------------------------------------------------------------------------------

	public String getName()
	{
		return name;
	}

	public ErrorHandler getHandler()
	{
		return new DefaultErrorHandler(this);
	}

	public void setHandler(ErrorHandler errorHandler)
	{
	}

	public State getState()
	{
		return State.STARTED;
	}

	public void append(LogEvent logEvent)
	{
	}

	public boolean ignoreExceptions()
	{
		return true;
	}

	public void initialize()
	{
	}

	public void start()
	{
	}

	public void stop()
	{
	}

	public boolean isStarted()
	{
		return true;
	}

	public boolean isStopped()
	{
		return false;
	}

	public Layout<? extends Serializable> getLayout()
	{
		return new Layout<Serializable>()
		{
			public void encode(LogEvent source, ByteBufferDestination destination)
			{
			}

			public byte[] getFooter()
			{
				return new byte[0];
			}

			public byte[] getHeader()
			{
				return new byte[0];
			}

			public byte[] toByteArray(LogEvent event)
			{
				return new byte[0];
			}

			public Serializable toSerializable(LogEvent event)
			{
				return "dummy";
			}

			public String getContentType()
			{
				return "";
			}

			public Map<String, String> getContentFormat()
			{
				return Collections.emptyMap();
			}
		};
	}
}
