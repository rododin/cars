/*
 * CharacterProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class CharacterProperty
	extends AbstractProperty <Character>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Character DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public CharacterProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public CharacterProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public CharacterProperty(final String name, final Character defaultValue)
	{
		super (name, defaultValue);
	}

	public CharacterProperty(final Long id, final String name, final Character defaultValue)
	{
		super (id, name, defaultValue);
	}

	public CharacterProperty(final String name, final Character defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public CharacterProperty(final Long id, final String name, final Character defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : value.charAt(0));
	}
}
