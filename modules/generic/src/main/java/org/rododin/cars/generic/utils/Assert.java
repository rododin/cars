/*
 * Assert.java
 */

package org.rododin.cars.generic.utils;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class Assert
{
	public static void assertNull(Object object)
	{
		if (object != null)
			throw new IllegalArgumentException();
	}

	public static void assertNull(Object object, String msg)
	{
		if (object != null)
			throw new IllegalArgumentException(msg);
	}

	public static void assertNotNull(Object object)
	{
		if (object == null)
			throw new NullPointerException();
	}

	public static void assertNotNull(Object object, String msg)
	{
		if (object == null)
			throw new NullPointerException(msg);
	}

	public static void assertTrue(boolean expression)
	{
		if (!expression)
			throw new IllegalArgumentException();
	}

	public static void assertTrue(boolean expression, String msg)
	{
		if (!expression)
			throw new IllegalArgumentException(msg);
	}

	public static void assertFalse(boolean expression)
	{
		if (expression)
			throw new IllegalArgumentException();
	}

	public static void assertFalse(boolean expression, String msg)
	{
		if (expression)
			throw new IllegalArgumentException(msg);
	}

	public static void fail()
	{
		throw new RuntimeException();
	}

	public static void fail(String msg)
	{
		throw new RuntimeException(msg);
	}
}
