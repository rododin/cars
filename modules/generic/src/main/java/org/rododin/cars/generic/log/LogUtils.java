/*
 * LogUtils.java
 */

package org.rododin.cars.generic.log;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.LoggerContext;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class LogUtils
{
	/**
	 * Updates log level for the specified logger.
	 * @param loggerName the name of the logger
	 * @param level      the new level
	 */
	public static void updateLevel(String loggerName, Level level)
	{
		final Logger logger = getLogger(loggerName);
		if (logger != null)
		{
			logger.setLevel(level);
		}
	}

	/**
	 * Adds an appender to the specified logger.
	 * @param loggerName the name of the logger
	 * @param appender   the new appender to be added
	 */
	public static void addAppender(String loggerName, Appender appender)
	{
		final Logger logger = getLogger(loggerName);
		if (logger != null)
		{
			appender.start();
			logger.addAppender(appender);
		}
	}

	/**
	 * Removes the given <code>appender</code> from the specified logger.
	 * @param loggerName the name of the logger
	 * @param appender   the appender to be removed
	 */
	public static void removeAppender(String loggerName, Appender appender)
	{
		final Logger logger = getLogger(loggerName);
		if (logger != null)
		{
			logger.removeAppender(appender);
		}
	}

	/**
	 * Removes all appenders from the specified logger
	 * @param loggerName the name of the logger
	 */
	public static void removeAllAppenders(String loggerName)
	{
		final Logger logger = getLogger(loggerName);
		if (logger != null)
		{
			for (Appender appender : logger.getAppenders().values())
				logger.removeAppender(appender);
		}
	}

	public static boolean isGreaterOrEqual(Level level1, Level level2)
	{
		return level1.equals(level2) || level1.isLessSpecificThan(level2);
	}

// Internal Service Methods --------------------------------------------------------------------------------------------

	private static Logger getLogger(String loggerName)
	{
		final LoggerContext context = (LoggerContext) LogManager.getContext(false);
		for (final Logger logger : context.getLoggers())
		{
			if (logger.getName().equals(loggerName))
			{
				return logger;
			}
		}
		return null;
	}
}
