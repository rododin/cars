/*
 * IntegerProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class IntegerProperty
	extends AbstractProperty <Integer>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Integer DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public IntegerProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public IntegerProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public IntegerProperty(final String name, final Integer defaultValue)
	{
		super (name, defaultValue);
	}

	public IntegerProperty(final Long id, final String name, final Integer defaultValue)
	{
		super (id, name, defaultValue);
	}

	public IntegerProperty(final String name, final Integer defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public IntegerProperty(final Long id, final String name, final Integer defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : Integer.parseInt(value));
	}
}
