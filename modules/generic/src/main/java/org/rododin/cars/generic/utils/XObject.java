/*
 * XObject.java
 */

package org.rododin.cars.generic.utils;

import java.util.concurrent.Callable;

import org.rododin.cars.generic.log.Log;

/**
 * Extended version of <code>{@link java.lang.Object}</code>.
 * To benefit the features you have to just list it within <code>implements</code>.
 *
 * @author Rod Odin
 */
public interface XObject
{
	default void tryCatch(Runnable routine, String msg)
	{
		tryCatchThrow(routine, msg, false);
	}

	default void tryCatchThrow(Runnable routine, String msg)
	{
		tryCatchThrow(routine, msg, true);
	}

	default void tryCatchThrow(Runnable routine, String msg, boolean reThrow)
	{
		try
		{
			routine.run();
		}
		catch (RuntimeException x)
		{
			Log.error(msg, x);
			if (reThrow)
				throw x;
		}
	}

	default <R> R tryCatch(Callable<R> routine, String msg)
	{
		return tryCatchThrow(routine, msg, false);
	}

	default <R> R tryCatchThrow(Callable<R> routine, String msg)
	{
		return tryCatchThrow(routine, msg, true);
	}

	default <R> R tryCatchThrow(Callable<R> routine, String msg, boolean reThrow)
	{
		try
		{
			return routine.call();
		}
		catch (Exception x)
		{
			Log.error(msg, x);
			if (reThrow)
			{
				if (x instanceof RuntimeException)
					throw (RuntimeException)x;
				else
					throw new RuntimeException(x);
			}
			else
				return null;
		}
	}

	default void trySleep(long millis)
	{
		try
		{
			Thread.sleep(millis);
		}
		catch (InterruptedException x)
		{
		}
	}

	default void tryWait()
	{
		try
		{
			wait();
		}
		catch (InterruptedException x)
		{
		}
	}

	default void tryWait(long timeout)
	{
		try
		{
			wait(timeout);
		}
		catch (InterruptedException x)
		{
		}
	}

	default void tryWait(long timeout, int nanos)
	{
		try
		{
			wait(timeout, nanos);
		}
		catch (InterruptedException x)
		{
		}
	}
}
