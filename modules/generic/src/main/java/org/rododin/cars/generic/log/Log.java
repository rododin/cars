/*
 * Log.java
 */

package org.rododin.cars.generic.log;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class Log
{

	private static final Logger LOG_INFO  = LogManager.getLogger("org.rododin.cars.generic.log.LogInfo");
	private static final Logger LOG_DEBUG = LogManager.getLogger("org.rododin.cars.generic.log.LogDebug");

	public static final Logger MAIN_LOG = LogManager.getLogger("org.rododin.cars");

	public static final Logger TRACE_LOG = MAIN_LOG;
	public static final Logger DEBUG_LOG = MAIN_LOG;
	public static final Logger INFO_LOG  = MAIN_LOG;
	public static final Logger WARN_LOG  = MAIN_LOG;
	public static final Logger ERROR_LOG = Log4JOutputStreamWrapper.STDERR_LOG;
	public static final Logger FATAL_LOG = Log4JOutputStreamWrapper.STDERR_LOG;

	public static final Logger FORCED_TRACE_LOG = LOG_DEBUG;
	public static final Logger FORCED_DEBUG_LOG = LOG_DEBUG;
	public static final Logger FORCED_INFO_LOG  = LOG_INFO;
	public static final Logger FORCED_WARN_LOG  = LOG_INFO;
	public static final Logger FORCED_ERROR_LOG = Log4JOutputStreamWrapper.STDERR_LOG;
	public static final Logger FORCED_FATAL_LOG = Log4JOutputStreamWrapper.STDERR_LOG;

// Getters -------------------------------------------------------------------------------------------------------------

	public static boolean isTraceEnabled()
	{
		return TRACE_LOG.isTraceEnabled();
	}

	public static boolean isDebugEnabled()
	{
		return DEBUG_LOG.isDebugEnabled();
	}

	public static boolean isInfoEnabled()
	{
		return INFO_LOG.isInfoEnabled();
	}

	public static boolean isWarnEnabled()
	{
		return WARN_LOG.isWarnEnabled();
	}

	public static boolean isErrorEnabled()
	{
		return ERROR_LOG.isErrorEnabled();
	}

	public static boolean isFatalEnabled()
	{
		return FATAL_LOG.isFatalEnabled();
	}

// Logging Methods -----------------------------------------------------------------------------------------------------

	public static void trace(Object message)
	{
		TRACE_LOG.trace(message);
	}

	public static void trace(Object message, Throwable t)
	{
		TRACE_LOG.trace(message, t);
	}

	public static void traceForced(Object message)
	{
		FORCED_TRACE_LOG.trace(message);
	}

	public static void traceForced(Object message, Throwable t)
	{
		FORCED_TRACE_LOG.trace(message, t);
	}

	public static void debug(Object message)
	{
		DEBUG_LOG.debug(message);
	}

	public static void debug(Object message, Throwable t)
	{
		DEBUG_LOG.debug(message, t);
	}

	public static void debugForced(Object message)
	{
		FORCED_DEBUG_LOG.debug(message);
	}

	public static void debugForced(Object message, Throwable t)
	{
		FORCED_DEBUG_LOG.debug(message, t);
	}

	public static void info(Object message)
	{
		INFO_LOG.info(message);
	}

	public static void info(Object message, Throwable t)
	{
		INFO_LOG.info(message, t);
	}

	public static void infoForced(Object message)
	{
		FORCED_INFO_LOG.info(message);
	}

	public static void infoForced(Object message, Throwable t)
	{
		FORCED_INFO_LOG.info(message, t);
	}

	public static void warn(Object message)
	{
		WARN_LOG.warn(message);
	}

	public static void warn(Object message, Throwable t)
	{
		WARN_LOG.warn(message, t);
	}

	public static void warnForced(Object message)
	{
		FORCED_WARN_LOG.warn(message);
	}

	public static void warnForced(Object message, Throwable t)
	{
		FORCED_WARN_LOG.warn(message, t);
	}

	public static void error(Object message)
	{
		ERROR_LOG.error(message);
	}

	public static void error(Object message, Throwable t)
	{
		ERROR_LOG.error(message, t);
	}

	public static void errorForced(Object message)
	{
		FORCED_ERROR_LOG.error(message);
	}

	public static void errorForced(Object message, Throwable t)
	{
		FORCED_ERROR_LOG.error(message, t);
	}

	public static void fatal(Object message)
	{
		FATAL_LOG.fatal(message);
	}

	public static void fatal(Object message, Throwable t)
	{
		FATAL_LOG.fatal(message, t);
	}

	public static void fatalForced(Object message)
	{
		FORCED_FATAL_LOG.fatal(message);
	}

	public static void fatalForced(Object message, Throwable t)
	{
		FORCED_FATAL_LOG.fatal(message, t);
	}
}
