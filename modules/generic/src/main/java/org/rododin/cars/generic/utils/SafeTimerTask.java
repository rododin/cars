/*
 * SafeTimerTask.java
 */

package org.rododin.cars.generic.utils;

import java.util.TimerTask;

import org.rododin.cars.generic.Constants;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class SafeTimerTask
	extends TimerTask
{
	@Override
	public void run()
	{
		try
		{
			doRun();
		}
		catch (Exception x)
		{
			//Constants.LOG.warn("TimerTask failed", x);
			x.printStackTrace();
		}
	}

	protected abstract void doRun();
}
