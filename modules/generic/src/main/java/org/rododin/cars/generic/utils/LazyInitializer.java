/*
 * LazyInitializer.java
 */

package org.rododin.cars.generic.utils;

import java.util.function.Supplier;

/**
 * Introduces a lazy-initialization helper.
 * On first access it initializes the value using the provided <code>{@link Supplier}.
 * The first access is synchronized, however after it's executed, the internal access controller get exchanged
 * with a non-synchronized version. Thus any next access expenses trivial getter costs only.
 *
 * Based on article "Singleton as Function Pointer": http://habrahabr.ru/post/228079/
 *
 * @param <V> the value type
 */
public class LazyInitializer<V>
{
	public LazyInitializer(final Supplier<V> initializer)
	{
		accessor = new InitializingGetter(initializer);
	}

	/**
	 * The access method.
	 * On first invocation it initializes the value using the <code>{@link Supplier} provided on to
	 * <code>{@link #LazyInitializer(Supplier)}</code>.
	 * On next invocations it just returns the value.
	 *
	 * @return the initialized value
	 */
	public V get()
	{
		return accessor.get();
	}

	public V getIfInitialized()
	{
		return accessor instanceof LazyInitializer.AccessGetter ? accessor.get() : null;
	}

	/**
	 * The access controller used only once to initialize the value on first access.
	 * When it gets once executed, it passes the access control to <code>{@link AccessGetter}</code>.
	 */
	private class InitializingGetter implements Supplier<V>
	{
		private final Supplier<V> initializer;

		private InitializingGetter(final Supplier<V> initializer)
		{
			this.initializer = initializer;
		}

		public synchronized V get()
		{
			if (accessor != this)
				return accessor.get();

			V instance = initializer.get();
			accessor = new AccessGetter(instance);
			return instance;
		}
	}

	/**
	 * The access controller used on 2nd and further access attempts.
	 * It just returns the previously initialized value.
	 * @see InitializingGetter
	 */
	private class AccessGetter implements Supplier<V>
	{
		private final V instance;

		private AccessGetter(final V instance)
		{
			this.instance = instance;
		}

		public V get()
		{
			return instance;
		}
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private Supplier<V> accessor;

// Testing Code --------------------------------------------------------------------------------------------------------

	// run with -XX:+UnlockDiagnosticVMOptions -XX:+PrintInlining to see functions inlining
	//public static void main(String[] args)
	//{
	//	final LazyInitializer<Integer> initializer = new LazyInitializer<>(() -> {
	//		System.err.println("INIT");
	//		return new Random().nextInt();
	//	});
	//
	//	System.err.println("START");
	//
	//	for (int i = 0; i < 1000; i++)
	//	{
	//		System.err.printf("%03d %s\n", i, initializer.get());
	//	}
	//}
}
