/*
 * StringProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class StringProperty
	extends AbstractProperty <String>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final String DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public StringProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public StringProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public StringProperty(final String name, final String defaultValue)
	{
		super (name, defaultValue);
	}

	public StringProperty(final Long id, final String name, final String defaultValue)
	{
		super (id, name, defaultValue);
	}

	public StringProperty(final String name, final String defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public StringProperty(final Long id, final String name, final String defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : value);
	}
}
