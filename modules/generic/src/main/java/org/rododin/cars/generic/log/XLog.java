/*
 * XLog.java
 */

package org.rododin.cars.generic.log;

import java.util.function.Supplier;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class XLog
	extends Log
{
// Unified Logging Methods ---------------------------------------------------------------------------------------------

	public static void log(Level level, Supplier messageSupplier)
	{
		log(MAIN_LOG, level, messageSupplier, "", null);
	}

	public static void log(Level level, Supplier messageSupplier, Throwable t)
	{
		log(MAIN_LOG, level, messageSupplier, "", t);
	}

	public static void log(Level level, Supplier messageSupplier, String prefix)
	{
		log(MAIN_LOG, level, messageSupplier, prefix, null);
	}

	public static void log(Level level, Supplier messageSupplier, String prefix, Throwable t)
	{
		if (isLoggerEnabledForLevel(MAIN_LOG, level))
			MAIN_LOG.log(level, prefix + messageSupplier.get(), t);
	}

	public static void log(Logger log, Level level, Supplier messageSupplier)
	{
		log(log, level, messageSupplier, "", null);
	}

	public static void log(Logger log, Level level, Supplier messageSupplier, Throwable t)
	{
		log(log, level, messageSupplier, "", t);
	}

	public static void log(Logger log, Level level, Supplier messageSupplier, String prefix)
	{
		log(log, level, messageSupplier, prefix, null);
	}

	public static void log(Logger log, Level level, Supplier messageSupplier, String prefix, Throwable t)
	{
		if (isLoggerEnabledForLevel(log, level))
			log.log(level, prefix + messageSupplier.get(), t);
	}

// Special Logging Methods ---------------------------------------------------------------------------------------------

	public static void trace(Supplier messageSupplier)
	{
		log(TRACE_LOG, Level.TRACE, messageSupplier);
	}

	public static void trace(Supplier messageSupplier, Throwable t)
	{
		log(TRACE_LOG, Level.TRACE, messageSupplier, t);
	}

	public static void traceForced(Supplier messageSupplier)
	{
		log(FORCED_TRACE_LOG, Level.TRACE, messageSupplier);
	}

	public static void traceForced(Supplier messageSupplier, Throwable t)
	{
		log(FORCED_TRACE_LOG, Level.TRACE, messageSupplier, t);
	}

	public static void debug(Supplier messageSupplier)
	{
		log(DEBUG_LOG, Level.DEBUG, messageSupplier);
	}

	public static void debug(Supplier messageSupplier, Throwable t)
	{
		log(DEBUG_LOG, Level.DEBUG, messageSupplier, t);
	}

	public static void debugForced(Supplier messageSupplier)
	{
		log(FORCED_DEBUG_LOG, Level.DEBUG, messageSupplier);
	}

	public static void debugForced(Supplier messageSupplier, Throwable t)
	{
		log(FORCED_DEBUG_LOG, Level.DEBUG, messageSupplier, t);
	}

	public static void info(Supplier messageSupplier)
	{
		log(INFO_LOG, Level.INFO, messageSupplier);
	}

	public static void info(Supplier messageSupplier, Throwable t)
	{
		log(INFO_LOG, Level.INFO, messageSupplier, t);
	}

	public static void infoForced(Supplier messageSupplier)
	{
		log(FORCED_INFO_LOG, Level.INFO, messageSupplier);
	}

	public static void infoForced(Supplier messageSupplier, Throwable t)
	{
		log(FORCED_INFO_LOG, Level.INFO, messageSupplier, t);
	}

	public static void warn(Supplier messageSupplier)
	{
		log(WARN_LOG, Level.WARN, messageSupplier);
	}

	public static void warn(Supplier messageSupplier, Throwable t)
	{
		log(WARN_LOG, Level.WARN, messageSupplier, t);
	}

	public static void warnForced(Supplier messageSupplier)
	{
		log(FORCED_WARN_LOG, Level.WARN, messageSupplier);
	}

	public static void warnForced(Supplier messageSupplier, Throwable t)
	{
		log(FORCED_WARN_LOG, Level.WARN, messageSupplier, t);
	}

	public static void error(Supplier messageSupplier)
	{
		log(ERROR_LOG, Level.ERROR, messageSupplier);
	}

	public static void error(Supplier messageSupplier, Throwable t)
	{
		log(ERROR_LOG, Level.ERROR, messageSupplier, t);
	}

	public static void errorForced(Supplier messageSupplier)
	{
		log(FORCED_ERROR_LOG, Level.ERROR, messageSupplier);
	}

	public static void errorForced(Supplier messageSupplier, Throwable t)
	{
		log(FORCED_ERROR_LOG, Level.ERROR, messageSupplier, t);
	}

	public static void fatal(Supplier messageSupplier)
	{
		log(FATAL_LOG, Level.FATAL, messageSupplier);
	}

	public static void fatal(Supplier messageSupplier, Throwable t)
	{
		log(FATAL_LOG, Level.FATAL, messageSupplier, t);
	}

	public static void fatalForced(Supplier messageSupplier)
	{
		log(FORCED_FATAL_LOG, Level.FATAL, messageSupplier);
	}

	public static void fatalForced(Supplier messageSupplier, Throwable t)
	{
		log(FORCED_FATAL_LOG, Level.FATAL, messageSupplier, t);
	}

// Internal Service Methods --------------------------------------------------------------------------------------------

	private static boolean isLoggerEnabledForLevel(Logger logger, Level level)
	{
		return !level.equals(Level.OFF)
		    && logger.getLevel().compareTo(level) >= 0;
	}
}
