/*
 * ReentrantLockHelperMultiple.java
 */

package org.rododin.cars.generic.utils.locks;

/**
 * Joins several <code>{@link ReentrantLockHelper}</code> instances together
 * and ensuring the order of locks acquiring and releasing is always the same.
 */
public class MultiReentrantLockHelper
	implements ReentrantLockHelper
{
// Constructors --------------------------------------------------------------------------------------------------------

	public MultiReentrantLockHelper(ReentrantLockHelper... lockers)
	{
		this.lockers = lockers;
	}

// Lock/Unlock Methods -------------------------------------------------------------------------------------------------

	public void lock(LockType lockType)
	{
		for (ReentrantLockHelper locker : lockers)
			locker.lock(lockType);
	}

	public void unlock(LockType lockType)
	{
		for (int i = lockers.length - 1; i >= 0; i--)
			lockers[i].unlock(lockType);
	}

// Attributes ----------------------------------------------------------------------------------------------------------

	private final ReentrantLockHelper[] lockers;
}
