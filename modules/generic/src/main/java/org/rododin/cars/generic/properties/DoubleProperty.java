/*
 * DoubleProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class DoubleProperty
	extends AbstractProperty <Double>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Double DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public DoubleProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public DoubleProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public DoubleProperty(final String name, final Double defaultValue)
	{
		super (name, defaultValue);
	}

	public DoubleProperty(final Long id, final String name, final Double defaultValue)
	{
		super (id, name, defaultValue);
	}

	public DoubleProperty(final String name, final Double defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public DoubleProperty(final Long id, final String name, final Double defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : Double.parseDouble(value));
	}
}
