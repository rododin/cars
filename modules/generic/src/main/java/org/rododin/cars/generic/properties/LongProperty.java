/*
 * LongProperty.java
 */

package org.rododin.cars.generic.properties;

/**
 * Description.
 * @author Rod Odin
 */
public class LongProperty
	extends AbstractProperty <Long>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Long DEFAULT_VALUE = null;

// Constructors --------------------------------------------------------------------------------------------------------

	public LongProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public LongProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public LongProperty(final String name, final Long defaultValue)
	{
		super (name, defaultValue);
	}

	public LongProperty(final Long id, final String name, final Long defaultValue)
	{
		super (id, name, defaultValue);
	}

	public LongProperty(final String name, final Long defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public LongProperty(final Long id, final String name, final Long defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : Long.parseLong(value));
	}
}
