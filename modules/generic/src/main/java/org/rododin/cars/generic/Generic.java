/*
 * Generic.java
 */

package org.rododin.cars.generic;

import org.rododin.cars.generic.log.Log;

/**
 * Main routine stub.
 *
 * @author Rod Odin
 */
public class Generic
{
	/** It's totally redundant here, keeping it for misc dev-time usage only. */
	public static void main(String[] args)
	{
		Log.infoForced("Cars - Generic");
	}
}
