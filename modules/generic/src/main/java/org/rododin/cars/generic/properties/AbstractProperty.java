/*
 * AbstractProperty.java
 */

package org.rododin.cars.generic.properties;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Description.
 * @author Rod Odin
 */
public abstract class AbstractProperty <Value>
	implements Property <Value>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final boolean DEFAULT_ENABLED   =  true;
	public static final boolean DEFAULT_WRITABLE  =  true;
	public static final boolean DEFAULT_IMPORTANT = false;

	public static final String  NULL_STRING_VALUE = "null";

// Constructors --------------------------------------------------------------------------------------------------------

	protected AbstractProperty(String name, Value defaultValue)
	{
		this (ID_GENERATOR.incrementAndGet(), name, defaultValue, DEFAULT_ENABLED, DEFAULT_WRITABLE, DEFAULT_IMPORTANT);
	}

	protected AbstractProperty(Long id, String name, Value defaultValue)
	{
		this (id, name, defaultValue, DEFAULT_ENABLED, DEFAULT_WRITABLE, DEFAULT_IMPORTANT);
	}

	protected AbstractProperty(String name, Value defaultValue, boolean enabled, boolean writable, boolean important)
	{
		this (ID_GENERATOR.incrementAndGet(), name, defaultValue, enabled, writable, important);
	}

	protected AbstractProperty(Long id, String name, Value defaultValue, boolean enabled, boolean writable, boolean important)
	{
		this.id = id;
		this.name = name;
		this.defaultValue = defaultValue;
		this.value = defaultValue;
		this.enabled = enabled;
		this.writable = writable;
		this.important = important;
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public Long getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public Value getDefaultValue()
	{
		return defaultValue;
	}

	public String getDefaultValueAsString()
	{
		return defaultValue != null ? defaultValue.toString() : NULL_STRING_VALUE;
	}

	public Value getValue()
	{
		return value;
	}

	public void setValue(final Value value)
	{
		this.value = value;
	}

	public String getValueAsString()
	{
		final Value value = this.value;
		return value != null ? value.toString() : NULL_STRING_VALUE;
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public void setEnabled(final boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean isWritable()
	{
		return writable;
	}

	public void setWritable(final boolean writable)
	{
		this.writable = writable;
	}

	public boolean isImportant()
	{
		return important;
	}

	public void setImportant(final boolean important)
	{
		this.important = important;
	}

// Overridden Methods --------------------------------------------------------------------------------------------------

	@Override
	public boolean equals(final Object o)
	{
		if (this == o)
			return true;
		if (!(o instanceof AbstractProperty))
			return false;

		final AbstractProperty that = (AbstractProperty) o;
		if (!id.equals(that.id))
			return false;
		if (!name.equals(that.name))
			return false;
		if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null)
			return false;
		final Value value = this.value;
		if (value != null ? !value.equals(that.value) : that.value != null)
			return false;
		if (enabled != that.enabled)
			return false;
		if (writable != that.writable)
			return false;
		if (important != that.important)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id.hashCode();
		result = 31 * result + name.hashCode();
		result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
		final Value value = this.value;
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + (enabled ? 1 : 0);
		result = 31 * result + (writable ? 1 : 0);
		result = 31 * result + (important ? 1 : 0);
		return result;
	}

// Internal Stuff ------------------------------------------------------------------------------------------------------

	private static final AtomicLong ID_GENERATOR = new AtomicLong();

	private final Long id;
	private final String name;
	private final Value defaultValue;
	private volatile Value value;
	private volatile boolean enabled;
	private volatile boolean writable;
	private volatile boolean important;
}
