/*
 * BooleanProperty.java
 */

package org.rododin.cars.generic.properties;

import java.util.HashSet;
import java.util.Set;

/**
 * Description.
 * @author Rod Odin
 */
public class BooleanProperty
	extends AbstractProperty <Boolean>
{
// Constants -----------------------------------------------------------------------------------------------------------

	public static final Boolean DEFAULT_VALUE = null;

	public static final Set<String> TRUE_STRING_VALUES = new HashSet<String>();
	static
	{
		TRUE_STRING_VALUES.add(Boolean.TRUE.toString());
		TRUE_STRING_VALUES.add("y");
		TRUE_STRING_VALUES.add("Y");
		TRUE_STRING_VALUES.add("yes");
		TRUE_STRING_VALUES.add("Yes");
		TRUE_STRING_VALUES.add("YES");
		TRUE_STRING_VALUES.add("on");
		TRUE_STRING_VALUES.add("On");
		TRUE_STRING_VALUES.add("ON");
		TRUE_STRING_VALUES.add("j");
		TRUE_STRING_VALUES.add("J");
		TRUE_STRING_VALUES.add("ja");
		TRUE_STRING_VALUES.add("Ja");
		TRUE_STRING_VALUES.add("JA");
		TRUE_STRING_VALUES.add("\u0434");
		TRUE_STRING_VALUES.add("\u0414");
		TRUE_STRING_VALUES.add("\u0434\u0430");
		TRUE_STRING_VALUES.add("\u0414\u0430");
		TRUE_STRING_VALUES.add("\u0414\u0410");
		// TODO: Add other languages
	}

// Constructors --------------------------------------------------------------------------------------------------------

	public BooleanProperty(final String name)
	{
		super (name, DEFAULT_VALUE);
	}

	public BooleanProperty(final Long id, final String name)
	{
		super (id, name, DEFAULT_VALUE);
	}

	public BooleanProperty(final String name, final Boolean defaultValue)
	{
		super (name, defaultValue);
	}

	public BooleanProperty(final Long id, final String name, final Boolean defaultValue)
	{
		super (id, name, defaultValue);
	}

	public BooleanProperty(final String name, final Boolean defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (name, defaultValue, enabled, writable, important);
	}

	public BooleanProperty(final Long id, final String name, final Boolean defaultValue, final boolean enabled, final boolean writable, final boolean important)
	{
		super (id, name, defaultValue, enabled, writable, important);
	}

// Getters/Setters -----------------------------------------------------------------------------------------------------

	public void setValueAsString(final String value)
	{
		setValue(value == null || NULL_STRING_VALUE.equalsIgnoreCase(value) ? null : TRUE_STRING_VALUES.contains(value));
	}
}
