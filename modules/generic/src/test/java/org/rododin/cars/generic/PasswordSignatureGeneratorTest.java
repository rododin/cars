/*
 * PasswordSignatureGeneratorTest.java
 */

package org.rododin.cars.generic;

import org.junit.Assert;
import org.junit.Test;
import org.rododin.cars.generic.log.Log;
import org.rododin.cars.generic.utils.PasswordSignatureGenerator;

/**
 * Description.
 * @author Rod Odin
 */
public class PasswordSignatureGeneratorTest
{
	@Test
	public void testSomeSignatures()
	{
		final String[] usernamesAndPasswords = new String[]
		{
			"RodOdin/testing/ca2911b345b22a2ea275f132bd414be7",
			"OdinRod/testing/6378ab75b8cff58e37bd6798fe0d057",
			"vader/force/c986c99ab3ab27576545c7c28555b1",
		};

		for (String up : usernamesAndPasswords)
		{
			final String[] strings = up.split("/");
			final String signature = PasswordSignatureGenerator.createSignature(strings[0], strings[1]);

			Assert.assertEquals(strings[2], signature);

			Log.debugForced(strings[0] + "/" + strings[1] + ": " + signature + " - OK!");
		}
	}
}
