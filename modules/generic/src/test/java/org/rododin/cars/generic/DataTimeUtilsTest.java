/*
 * DataTimeUtilsTest.java
 */

package org.rododin.cars.generic;

import java.text.ParseException;
import java.util.Date;

import org.junit.Assert;
import org.junit.Test;
import org.rododin.cars.generic.log.Log;
import org.rododin.cars.generic.utils.DateTimeUtils;

/**
 * Description.
 * @author Rod Odin
 */
public class DataTimeUtilsTest
{
	@Test
	public void testDate()
		throws ParseException
	{
		final String testDateStr = "2000.01.01";
		final Date date = DateTimeUtils.toDate(testDateStr);
		final String dateStr = DateTimeUtils.fromDate(date);
		Assert.assertEquals(testDateStr, dateStr);

		Log.debugForced(testDateStr + " - OK!");
	}

	@Test
	public void testTime()
		throws ParseException
	{
		final String testTimeStr = "19:01:22";
		final Date time = DateTimeUtils.toTime(testTimeStr);
		final String timeStr = DateTimeUtils.fromTime(time);
		Assert.assertEquals(testTimeStr, timeStr);

		Log.debugForced(testTimeStr + " - OK!");
	}

	@Test
	public void testDateTime()
		throws ParseException
	{
		final String testDateTimeStr = "2012.07.11-15:17:29";
		final Date dateTime = DateTimeUtils.toDateTime(testDateTimeStr);
		final String dateTimeStr = DateTimeUtils.fromDateTime(dateTime);
		Assert.assertEquals(testDateTimeStr, dateTimeStr);

		Log.debugForced(testDateTimeStr + " - OK!");
	}
}
